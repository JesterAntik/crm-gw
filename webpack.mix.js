const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/clients.js', 'public/js')
    .js('resources/js/deals.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

if (!mix.inProduction()) {
    mix.webpackConfig({
        devtool: 'source-map'
    })
        .sourceMaps()
}

mix.extract([
    'vue',
    'bootstrap-vue',
    'lodash',
    'bootstrap',
    'jquery',
    'popper.js',
]);

mix.version();


mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery']
});
