<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(['id'=>1],[
            'name' => 'Admin',
            'email' => 'admin@ptvk.me',
            'password' => Hash::make(config('app.admin_password')),
            'api_token' => Hash::make(config('app.admin_password').env('APP_KEY')),
        ]);
    }
}
