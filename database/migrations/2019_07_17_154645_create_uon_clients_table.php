<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUONClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uon_clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("global_u_id")->index();
            $table->integer("u_id")->index();
            $table->tinyInteger("u_type")->nullable()->description('Тип записи (1 - физ.лицо, 2 - юр.лицо, 3 - турагентство) / Record type (1 - individual, 2 - Jur.person 3 - travel Agency)');
            $table->tinyInteger("tourist_kind")->nullable()->description('(1 - Mr, 2 - Mrs, 3 - Miss, 4 - Child, 5 - Infant)');
            $table->string("u_surname")->index()->nullable();
            $table->string("u_name")->index()->nullable();
            $table->string("u_sname")->index()->nullable();
            $table->json('clean_result')->nullable();
            $table->string("u_surname_en")->index()->nullable();
            $table->string("u_name_en")->index()->nullable();

            $table->string("u_email")->index()->nullable();
            $table->tinyInteger("u_sex")->nullable();
            $table->string("u_fax")->nullable();
            $table->string("u_phone")->index()->nullable();
            $table->string("u_phone_mobile")->index()->nullable();
            $table->string("u_phone_home")->index()->nullable();

            $table->string("u_passport_number")->nullable();
            $table->string("u_passport_taken")->nullable();
            $table->string("u_passport_date")->nullable();

            $table->string("u_zagran_number")->nullable();
            $table->string("u_zagran_given")->nullable();
            $table->string("u_zagran_expire")->nullable();
            $table->string("u_zagran_organization")->nullable();

            $table->string("u_birthday")->nullable();
            $table->string("u_birthday_place")->nullable();
            $table->string("u_birthday_certificate")->nullable();



            $table->integer("manager_id")->index()->nullable();
            $table->integer("u_manager_id")->index()->nullable();

            $table->string("u_social_vk")->nullable();
            $table->string("u_social_fb")->nullable();
            $table->string("u_social_ok")->nullable();
            $table->string("u_telegram")->nullable();
            $table->string("u_whatsapp")->nullable();
            $table->string("u_viber")->nullable();
            $table->string("u_instagram")->nullable();
            $table->string("u_discount_card_number")->nullable();
            $table->string("u_discount_card_bonus")->nullable();

            $table->string("u_image")->nullable();
            $table->text("u_note")->nullable();
            $table->string("timezone")->nullable();
            $table->string("u_position")->nullable();

            $table->tinyInteger("u_manager_status")->nullable();
            $table->string("u_company")->nullable();
            $table->string("u_inn")->nullable();
            $table->string("u_kpp")->nullable();
            $table->string("u_okved")->nullable();
            $table->string("u_ogrn")->nullable();
            $table->string("address")->nullable();
            $table->string("address_juridical")->nullable();
            $table->string("u_finance_bank")->nullable();
            $table->string("u_finance_rs")->nullable();
            $table->string("u_finance_ks")->nullable();
            $table->string("u_finance_bik")->nullable();
            $table->string("u_finance_okpo")->nullable();
            $table->string("delivery_subscription")->nullable();
            $table->tinyInteger("role_id")->nullable();
            $table->tinyInteger("office_id")->nullable();
            $table->tinyInteger("u_office_id")->nullable();
            $table->boolean("active")->default(true);
            $table->boolean("deleted")->default(false);
            $table->string("ip_internal_number")->nullable();
            $table->string("labels")->nullable();
            $table->string("u_labels")->nullable();
            $table->string("u_date_update")->nullable();
            $table->boolean("cabinet_created")->default(false);
            $table->string("cabinet_created_date")->nullable();
            $table->smallInteger("nationality_id")->nullable();
            $table->string("nationality")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uon_clients');
    }
}
