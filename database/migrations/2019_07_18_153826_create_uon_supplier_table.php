<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUonSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uon_suppliers', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('name', 100);
            $table->string('name_official', 100);
            $table->integer('type_id');
            $table->string('address')->nullable();
            $table->string('phones')->nullable();
            $table->string('inn')->nullable();
            $table->string('email')->nullable();
            $table->string('contacts')->nullable();
            $table->text('note')->nullable();
            $table->boolean('is_active')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uon_suppliers');
    }
}
