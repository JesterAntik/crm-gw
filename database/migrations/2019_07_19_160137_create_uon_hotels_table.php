<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUONHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uon_hotels', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->primary();
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('stars',50)->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->tinyInteger('bedroom_count')->unsigned()->nullable();
            $table->tinyInteger('guests_count')->unsigned()->nullable();
            $table->decimal('price',50,2)->nullable();
            $table->text('text')->nullable();
            $table->text('reserve_rules')->nullable();
            $table->string('exit_hour')->nullable();
            $table->string('contacts')->nullable();
            $table->text('notice')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uon_hotels');
    }
}
