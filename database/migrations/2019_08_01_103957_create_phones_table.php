<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trim_phone')->unique();
            $table->string('full_phone')->nullable();
            $table->string('type', 50)->nullable()->description('Тип телефона');
            $table->string('phone', 50)->nullable()->description('Стандартизованный телефон одной строкой');
            $table->string('country_code', 5)->nullable()->description('Код страны');
            $table->string('city_code', 5)->nullable()->description('Код города / DEF-код');
            $table->string('number', 10)->nullable()->description('Локальный номер телефона');
            $table->string('extension', 10)->nullable()->description('Добавочный номер');
            $table->string('provider', 100)->nullable()->description('Оператор связи (только для России)');
            $table->string('country', 50)->nullable()->description('Страна');
            $table->string('region', 100)->nullable()->description('Регион (только для России)');
            $table->string('city', 100)->nullable()->description('Город (только для стационарных телефонов)');
            $table->string('timezone', 13)->nullable()->description('Часовой пояс города для Росии, часовой пояс страны — для иностранных телефонов. Если у страны несколько поясов, вернёт минимальный и максимальный через слеш: UTC+5/UTC+6');
            $table->string('qc_conflict', 5)->nullable()->description('Признак конфликта телефона с адресом');
            $table->string('qc', 5)->nullable()->description('Код проверки');
        });

        Schema::table('uon_clients', function(Blueprint $table){
            $table->dropColumn('u_phone');
            $table->dropColumn('u_phone_mobile');
            $table->dropColumn('u_phone_home');

            $table->bigInteger('phone_id')->nullable();
            $table->bigInteger('phone_mobile_id')->nullable();
            $table->bigInteger('phone_home_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phones');
    }
}
