<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUONLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uon_leads', function (Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->bigInteger('id_system')->index();
            $table->string('id_internal')->index()->nullable();
            $table->string('reservation_number')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->timestamp('dat')->nullable();
            $table->timestamp('dat_lead')->nullable();
            $table->timestamp('dat_close')->nullable();
            $table->integer('office_id')->nullable();
            $table->integer('manager_id')->nullable();
            $table->integer('visa_id')->nullable();
            $table->integer('insurance_id')->nullable();
            $table->bigInteger('client_id')->nullable();
            $table->string('date_begin')->nullable();
            $table->string('date_end')->nullable();
            $table->integer('source_id')->nullable();
            $table->integer('travel_type_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('reason_deny_id')->nullable();
            $table->decimal('calc_price_netto', 50,2)->nullable();
            $table->decimal('calc_price', 50,2)->nullable();
            $table->integer('r_calc_partner_currency_id')->nullable();
            $table->integer('r_calc_client_currency_id')->nullable();
            $table->decimal('calc_increase',50,2)->nullable();
            $table->decimal('calc_decrease',50,2)->nullable();
            $table->decimal('calc_client',50,2)->nullable();
            $table->decimal('calc_partner',50,2)->nullable();

            $table->string('client_requirements_country_ids')->nullable();
            $table->string('client_requirements_date_from')->nullable();
            $table->string('client_requirements_date_to')->nullable();
            $table->string('client_requirements_days_from')->nullable();
            $table->string('client_requirements_days_to')->nullable();
            $table->string('client_requirements_hotel_stars')->nullable();
            $table->string('client_requirements_nutrition_ids')->nullable();
            $table->string('client_requirements_tourists_adult_count')->nullable();
            $table->string('client_requirements_tourists_child_count')->nullable();
            $table->string('client_requirements_tourists_child_age')->nullable();
            $table->string('client_requirements_tourists_baby_count')->nullable();
            $table->string('client_requirements_budget')->nullable();
            $table->text('client_requirements_note')->nullable();

            $table->string('payment_deadline_client')->nullable();

            $table->timestamp('dat_request')->nullable();
            $table->timestamp('dat_updated')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->integer('created_by_manager')->unsigned()->nullable();
            $table->text('notes')->nullable();
            $table->integer('bonus_limit')->nullable();
            $table->integer('company_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uon_leads');
    }
}
