<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailFlagsToUonClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('uon_clients', function (Blueprint $table) {
            $table->json('email_validation')->nullable()->after('u_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('uon_clients', function (Blueprint $table) {
            $table->dropColumn('email_validation');
        });
    }
}
