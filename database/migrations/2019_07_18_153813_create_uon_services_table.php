<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUonServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uon_services', function (Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->bigInteger('request_id');
            $table->bigInteger('service_id')->nullable();
            $table->dateTimeTz('date_begin')->nullable()->description('Дата начала услуги, формат: Y-m-d');
            $table->dateTimeTz('date_end')->nullable()->description('Дата окончания услуги, формат: Y-m-d');
            $table->text('description')->nullable();
            $table->boolean('in_package')->default(false)->description('Входит в состав пакетного тура (1) или нет (0)');
            $table->integer('service_type_id');
            $table->string('course')->nullable();
            $table->string("duration")->nullable()->description('Длительность');
            $table->decimal("price_netto", 50, 2)->nullable()->description('Себестоимость');
            $table->integer("rate_netto")->nullable()->description('Неизвестно что такое');
            $table->decimal("price", 50, 2)->nullable()->description('Стоимость клиенту (итоговая)');
            $table->integer("rate")->nullable()->description('Неизвестно что такое');
            $table->integer('currency_id')->description('ID валюты');
            $table->integer('currency_id_netto')->description('ID валюты')->nullable();;

            $table->decimal("discount", 50, 2)->nullable();
            $table->float('discount_in_percent', 5, 2);
            $table->tinyInteger('tourists_count')->unsigned()->default(2);
            $table->tinyInteger('tourists_child_count')->unsigned()->default(0);
            $table->tinyInteger('tourists_baby_count')->unsigned()->default(0);
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('hotel_id')->unsigned()->nullable();
            $table->integer('nutrition_id')->unsigned()->nullable();
            $table->integer('partner_id')->unsigned()->nullable();


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uon_services');
    }
}
