<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUonRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uon_requests', function (Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->bigInteger('id_system')->index();
            $table->string('id_internal')->index()->description('Номер заявки');
            $table->string('reservation_number')->nullable()->description('Номер заявки у туроператора');
            $table->integer('supplier_id')->nullable();

            $table->timestamp('dat')->nullable()->description('Дата создания, формат: Y-m-d H:i:s');
            $table->timestamp('dat_request')->nullable()->description('Неизвестная дата, формат: Y-m-d H:i:s');
            $table->timestamp('dat_lead')->nullable()->description('Неизвестная дата, формат: Y-m-d H:i:s');
            $table->string('dat_updated')->nullable();
            $table->string('dat_close')->nullable();
            $table->string('created_at')->nullable();

            $table->tinyInteger('office_id')->unsigned()->nullable();
            $table->integer('manager_id')->unsigned()->nullable();
            $table->integer('visa_id')->unsigned()->nullable();
            $table->integer('insurance_id')->unsigned()->nullable();
            $table->unsignedBigInteger('client_id');
            $table->timestamp('date_begin')->nullable()->description('Дата начала заявки, формат: Y-m-d H:i:s ');
            $table->timestamp('date_end')->nullable()->description('Дата окончания заявки, формат: Y-m-d H:i:s');
            $table->integer('source_id')->nullable();
            $table->integer('travel_type_id')->nullable();
            $table->integer('status_id')->nullable();

            $table->decimal('calc_price_netto',50,2)->nullable();
            $table->decimal('calc_price',50,2)->nullable();
            $table->integer('r_calc_partner_currency_id')->nullable();
            $table->integer('r_calc_client_currency_id')->nullable();

            $table->decimal('calc_increase',50,2)->nullable();
            $table->decimal('calc_decrease',50,2)->nullable();
            $table->decimal('calc_client',50,2)->nullable();
            $table->decimal('calc_partner',50,2)->nullable();

            $table->string('client_requirements_country_ids')->nullable();
            $table->string('client_requirements_date_from')->nullable();
            $table->string('client_requirements_date_to')->nullable();
            $table->string('client_requirements_days_from')->nullable();
            $table->string('client_requirements_days_to')->nullable();
            $table->string('client_requirements_hotel_stars')->nullable();
            $table->string('client_requirements_nutrition_ids')->nullable();
            $table->string('client_requirements_tourists_adult_count')->nullable();
            $table->string('client_requirements_tourists_child_count')->nullable();
            $table->string('client_requirements_tourists_child_age')->nullable();
            $table->string('client_requirements_tourists_baby_count')->nullable();
            $table->string('client_requirements_budget')->nullable();
            $table->text('client_requirements_note')->nullable();
            $table->string('payment_deadline_client')->nullable();
            $table->integer('reason_deny_id')->nullable();



            $table->bigInteger('created_by_manager')->nullable();
            $table->text('notes')->nullable();
            $table->integer('bonus_limit')->nullable();
            $table->integer('company_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uon_requests');
    }
}
