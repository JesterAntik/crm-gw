<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hash',40)->unique();
            $table->text('full_address');
            $table->text('result')->nullable()->description('Стандартизованный адрес одной строкой ');
            $table->string('postal_code', 6)->nullable()->description('Индекс');
            $table->string('country', 120)->nullable()->description('Страна');
            $table->string('country_iso_code', 2)->nullable()->description('ISO-код страны');
            $table->string('federal_district', 20)->nullable()->description('Федеральный округ');
            $table->string('region_fias_id', 36)->nullable()->description('ФИАС-код региона');
            $table->string('region_kladr_id', 19)->nullable()->description('КЛАДР-код региона');
            $table->string('region_iso_code', 6)->nullable()->description('ISO-код региона');
            $table->string('region_with_type', 131)->nullable()->description('Регион с типом');
            $table->string('region_type', 10)->nullable()->description('Тип региона (сокращенный)');
            $table->string('region_type_full', 50)->nullable()->description('Тип региона');
            $table->string('region', 120)->nullable()->description('Регион');
            $table->string('area_fias_id', 36)->nullable()->description('ФИАС - код района');
            $table->string('area_kladr_id', 19)->nullable()->description('КЛАДР - код района');
            $table->string('area_with_type', 131)->nullable()->description('Район в регионе с типом');
            $table->string('area_type', 10)->nullable()->description('Тип района в регионе(сокращенный)');
            $table->string('area_type_full', 50)->nullable()->description('Тип района в регионе');
            $table->string('area', 120)->nullable()->description('Район в регионе');
            $table->string('city_fias_id', 36)->nullable()->description('ФИАС - код города');
            $table->string('city_kladr_id', 19)->nullable()->description('КЛАДР - код города');
            $table->string('city_with_type', 131)->nullable()->description('Город с типом');
            $table->string('city_type', 10)->nullable()->description('Тип города(сокращенный)');
            $table->string('city_type_full', 50)->nullable()->description('Тип города');
            $table->string('city', 120)->nullable()->description('Город');
            $table->string('city_area', 120)->nullable()->description('Административный округ(только для Москвы)');
            $table->string('city_district_fias_id', 36)->nullable()->description('ФИАС - код района города(заполняется, только если район есть в ФИАС)');
            $table->string('city_district_kladr_id', 19)->nullable()->description('КЛАДР - код района города(не заполняется)');
            $table->string('city_district_with_type', 131)->nullable()->description('Район города с типом');
            $table->string('city_district_type', 10)->nullable()->description('Тип района города(сокращенный)');
            $table->string('city_district_type_full', 50)->nullable()->description('Тип района города');
            $table->string('city_district', 120)->nullable()->description('Район города');
            $table->string('settlement_fias_id', 36)->nullable()->description('ФИАС - код населенного пункта');
            $table->string('settlement_kladr_id', 19)->nullable()->description('КЛАДР - код населенного пункта');
            $table->string('settlement_with_type', 131)->nullable()->description('Населенный пункт с типом');
            $table->string('settlement_type', 10)->nullable()->description('Тип населенного пункта(сокращенный)');
            $table->string('settlement_type_full', 50)->nullable()->description('Тип населенного пункта');
            $table->string('settlement', 120)->nullable()->description('Населенный пункт');
            $table->string('street_fias_id', 36)->nullable()->description('ФИАС - код улицы');
            $table->string('street_kladr_id', 19)->nullable()->description('КЛАДР - код улицы');
            $table->string('street_with_type', 131)->nullable()->description('Улица с типом');
            $table->string('street_type', 10)->nullable()->description('Тип улицы(сокращенный)');
            $table->string('street_type_full', 50)->nullable()->description('Тип улицы');
            $table->string('street', 120)->nullable()->description('Улица');
            $table->string('house_fias_id', 36)->nullable()->description('ФИАС - код дома');
            $table->string('house_kladr_id', 19)->nullable()->description('КЛАДР - код дома');
            $table->string('house_type', 10)->nullable()->description('Тип дома(сокращенный)');
            $table->string('house_type_full', 50)->nullable()->description('Тип дома');
            $table->string('house', 50)->nullable()->description('Дом');
            $table->string('block_type', 10)->nullable()->description('Тип корпуса / строения(сокращенный)');
            $table->string('block_type_full', 50)->nullable()->description('Тип корпуса / строения');
            $table->string('block', 50)->nullable()->description('Корпус / строение');
            $table->string('flat_type', 10)->nullable()->description('Тип квартиры(сокращенный)');
            $table->string('flat_type_full', 50)->nullable()->description('Тип квартиры');
            $table->string('flat', 50)->nullable()->description('Квартира');
            $table->string('flat_area', 50)->nullable()->description('Площадь квартиры');
            $table->string('square_meter_price', 50)->nullable()->description('Рыночная стоимость м²');
            $table->string('flat_price', 50)->nullable()->description('Рыночная стоимость квартиры');
            $table->string('postal_box', 50)->nullable()->description('Абонентский ящик');
            $table->string('fias_id', 36)->nullable()->description('ФИАС - код адреса(идентификатор ФИАС) HOUSE . HOUSEGUID — если дом найден в ФИАС; ADDROBJ . AOGUID — в противном случае');
            $table->string('fias_code', 23)->nullable()->description('Иерархический код адреса в ФИАС(СС + РРР + ГГГ + ППП + СССС + УУУУ + ДДДД)');
            $table->tinyInteger('fias_level')->nullable()->description('Уровень детализации, до которого адрес найден в ФИАС;; 0 — страна; 1 — регион; 3 — район; 4 — город; 5 — район города; 6 — населенный пункт; 7 — улица; 8 — дом; 65 — планировочная структура; 90 — доп . территория; 91 — улица в доп . территории;  -1 — иностранный или пустой');
            $table->tinyInteger('fias_actuality_state')->nullable()->description('Признак актуальности адреса в ФИАС; 0 — актуальный; 1 - 50 — переименован; 51 — переподчинен; 99 — удален');
            $table->string('kladr_id', 19)->nullable()->description('КЛАДР - код адреса');
            $table->tinyInteger('capital_marker')->nullable()->description('Признак центра района или региона. 1 — центр района 2 — центр региона 3 — центр района и региона 4 — центральный район региона  0 — ничего из перечисленного');
            $table->string('okato', 11)->nullable()->description('Код ОКАТО');
            $table->string('oktmo', 11)->nullable()->description('Код ОКТМО');
            $table->string('tax_office', 4)->nullable()->description('Код ИФНС для физических лиц');
            $table->string('tax_office_legal', 4)->nullable()->description('Код ИФНС для организаций');
            $table->string('timezone', 13)->nullable()->description('Часовой пояс города для Росии, часовой пояс страны — для иностранных адресов . Если у страны несколько поясов, вернёт минимальный и максимальный через слеш: UTC + 5 / UTC + 6');
            $table->string('geo_lat', 12)->nullable()->description('Координаты: широта');
            $table->string('geo_lon', 12)->nullable()->description('Координаты: долгота');
            $table->string('beltway_hit', 8)->nullable()->description('Внутри кольцевой ?IN_MKAD — внутри МКАД(Москва);OUT_MKAD — за МКАД(Москва и область);IN_KAD — внутри КАД(Санкт - Петербург);OUT_KAD — за КАД(Санкт - Петербург и область);пусто — в остальных случаях;');
            $table->string('beltway_distance', 3)->nullable()->description('Расстояние от кольцевой в км .Заполнено, только если beltway_hit = OUT_MKAD или OUT_KAD, иначе пустое');
            $table->string('qc_geo', 5)->nullable()->description('Код точности координат');
            $table->string('qc_complete', 5)->nullable()->description('Код пригодности к рассылке');
            $table->string('qc_house', 5)->nullable()->description('Признак наличия дома в ФИАС');
            $table->string('qc', 5)->nullable()->description('Код проверки адреса');
            $table->string('unparsed_parts', 250)->nullable()->description('Нераспознанная часть адреса .Для адреса «Москва, Митинская улица, 40, вход с торца» вернет «ВХОД, С, ТОРЦА»');
            $table->json('metro')->nullable()->description('Список ближайших станций метро(до трёх штук)');
        });

        Schema::table('uon_clients', function(Blueprint $table){
            $table->dropColumn('address');
            $table->dropColumn('address_juridical');

            $table->bigInteger('address_id')->nullable();
            $table->bigInteger('legal_address_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
