<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUONCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uon_companies', function (Blueprint $table) {
            $table->integer('id');
            $table->string('name')->nullable();
            $table->string('fullname')->nullable();
            $table->string('name_rus')->nullable();
            $table->string('inn')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('u_o_n_companies');
    }
}
