<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address')->unique();
            $table->string('alt')->nullable();
            $table->boolean('is_valid')->nullable();
            $table->boolean('is_real')->default(false);
        });

        Schema::create('client_email',function(Blueprint $table){
            $table->bigInteger('client_id')->index();
            $table->bigInteger('email_id')->index();
        });
        Schema::table('uon_clients', function(Blueprint $table){
            $table->dropColumn('u_email');
            $table->dropColumn('email_validation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
