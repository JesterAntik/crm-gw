<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('uon_clients', function(Blueprint $table){
            $table->date('passport_date')->after('u_passport_date')->nullable();
            $table->date('zagran_given')->after('u_zagran_given')->nullable();
            $table->date('zagran_expired')->after('u_zagran_expired')->nullable();
            $table->date('birthday')->after('u_birthday')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
