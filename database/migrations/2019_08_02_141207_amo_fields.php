<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('amo_fields', function(Blueprint $table){
            $table->increments('id');
            $table->string('code')->index();
            $table->string('name');
            $table->json('values')->nullable();
            $table->tinyInteger('type');
            $table->tinyInteger('element_type');
            $table->boolean('is_active')->default(true);
            $table->boolean('is_deleted')->default(false);
            $table->bigInteger('amo_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('amo_fields');
    }
}
