<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUONFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uon_flights', function (Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->timestamp('date_begin')->nullable();
            $table->timestamp('date_end')->nullable();
            $table->time('time_begin')->nullable();
            $table->time('time_end')->nullable();
            $table->string('flight_number')->nullable();
            $table->string('course_begin')->nullable();
            $table->string('course_end')->nullable();
            $table->string('terminal_begin')->nullable();
            $table->string('terminal_end')->nullable();
            $table->string('code_begin')->nullable();
            $table->string('code_end')->nullable();
            $table->string('seats')->nullable();
            $table->string('tickets')->nullable();
            $table->string('type')->nullable();
            $table->string('class')->nullable();
            $table->string('duration')->nullable();
            $table->integer('supplier_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uon_flights');
    }
}
