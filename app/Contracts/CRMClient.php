<?php


namespace App\Contracts;


use App\Models\Client;

interface CRMClient
{
    public function getClients():array;

    public function getDeals($client_id= null):array;

    public function getLeads($client_id= null):array;
    
    public function setClients(array $clients);

    public function setDeals(array $deals);

    public function setLeads(array $leads);
}
