<?php


namespace App\Contracts;

/**
 * Interface CRMExport
 * @package App\Contracts
 */
interface CRMExport
{
    /**
     * @return void
     */
    public function exportClients();

    /**
     * @return void
     */
    public function exportLeads();

    /**
     * @return void
     */
    public function exportDeals();

    /**
     * @param string $listName
     * @return void
     */
    public function exportList($listName);
}
