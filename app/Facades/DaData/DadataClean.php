<?php

namespace App\Facades\DaData;

class DadataClean extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'dadata_clean';
    }
}
