<?php

namespace App\Http\Controllers;

use Auth;

class UONController extends Controller
{
    public function clients()
    {
        return view('uon.clients', ['initData' => [
            'user' => Auth::user(),
        ]]);
    }

    public function incorrectDeals()
    {
        return view('uon.deals', ['initData' => [
            'user' => Auth::user(),
            'deals' => [
                'failed' => true,
                ]
        ]]);
    }

    public function deals()
    {
        return view('uon.deals', ['initData' => [
            'user' => Auth::user(),
        ]]);
    }
}
