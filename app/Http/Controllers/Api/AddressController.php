<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ModifyRequest;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    public function update(ModifyRequest $request, $phone_id)
    {
        $address = Address::findOrFail($phone_id);

        foreach ($request->input('fields') as $field) {
            $address->setAttribute($field['key'],$field['value']);
        }
        $address->save();
        return new AddressResource($address);
    }
}
