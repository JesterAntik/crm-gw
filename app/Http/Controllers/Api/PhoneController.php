<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ModifyRequest;
use App\Http\Resources\PhoneResource;
use App\Models\Phone;
use App\Http\Controllers\Controller;

class PhoneController extends Controller
{
    public function update(ModifyRequest $request, $phone_id)
    {
        $phone = Phone::findOrFail($phone_id);

        foreach ($request->input('fields') as $field) {
            $phone->setAttribute($field['key'],$field['value']);
        }
        $phone->save();
        return new PhoneResource($phone);
    }
}
