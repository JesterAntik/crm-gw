<?php

namespace App\Http\Controllers\Api\UON;

use App\Http\Requests\ModifyRequest;
use App\Http\Resources\UON\ClientResourceCollection;
use App\Http\Resources\UON\DealResource;
use App\Http\Resources\UON\DealResourceCollection;
use App\Models\UONDeal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealController extends Controller
{
    public function index(Request $request)
    {
        $orderBy = $request->input('sortBy','id');
        $orderBy = empty($orderBy)?'id':$orderBy;
        $orderDirection = $request->input('sortDesc', 'false');
        $orderDirection = $orderDirection=="true"?'DESC':"ASC";
        $deals = UONDeal::with([
            'services',
            'supplier',
        ])
            ->orderBy($orderBy,$orderDirection)->paginate();
        // dd($clients->first()->manager);
        return new DealResourceCollection($deals);
    }

    public function failed(Request $request)
    {
        $orderBy = $request->input('sortBy','id');
        $orderBy = empty($orderBy)?'id':$orderBy;
        $orderDirection = $request->input('sortDesc', 'false');
        $orderDirection = $orderDirection=="true"?'DESC':"ASC";
        $deals = UONDeal::with('services')
            ->whereHas('services', function($query){
                $query->where([
                    ['description','<>',''],
                 //   ['service_type_id','in',[1,5,6,7,8,9,10,11,12,13,14,15,16,116]]
                ]);
            })
            ->orderBy($orderBy,$orderDirection)
            ->paginate();
        // dd($clients->first()->manager);
        return new DealResourceCollection($deals);
    }

    public function update(ModifyRequest $request, $id)
    {

        $deal = UONDeal::firstOrFail($id);

        foreach ($request->input('fields') as $field) {
            $deal->setAttribute($field['key'],$field['value']);
        }
        $deal->save();

        return new DealResource($deal);
    }
}
