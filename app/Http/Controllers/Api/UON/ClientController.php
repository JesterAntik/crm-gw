<?php

namespace App\Http\Controllers\Api\UON;

use App\Http\Requests\ModifyRequest;
use App\Http\Resources\UON\ClientResource;
use App\Http\Resources\UON\ClientResourceCollection;
use App\Models\UONClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $orderBy = $request->input('sortBy','id');
        $orderBy = empty($orderBy)?'id':$orderBy;
        $orderDirection = $request->input('sortDesc', 'false');
        $orderDirection = $orderDirection=="true"?'DESC':"ASC";
        $clients = UONClient::with('manager')->whereHas('emails')->orderBy($orderBy,$orderDirection)->paginate();
       // dd($clients->first()->manager);
        return new ClientResourceCollection($clients);
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $client = UONClient::findOrFail($id);
        return new ClientResource($client);
    }

    public function update(ModifyRequest $request, $id)
    {

        $client = UONClient::where('u_id',$id)->first();
        if (empty($client)) abort(404);

        foreach ($request->input('fields') as $field) {
            $client->setAttribute($field['key'],$field['value']);
        }
        $client->save();

        return new ClientResource($client);
    }

    public function destroy($id)
    {
        //
    }
}
