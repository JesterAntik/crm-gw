<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ModifyRequest;
use App\Http\Resources\EmailResource;
use App\Models\Email;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
    public function update(ModifyRequest $request, $email_id)
    {
        $email = Email::findOrFail($email_id);

        foreach ($request->input('fields') as $field) {
            $email->setAttribute($field['key'],$field['value']);
        }
        $email->save();
        return new EmailResource($email);
    }
}
