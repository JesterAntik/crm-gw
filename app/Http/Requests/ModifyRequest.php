<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entity' => 'string',
            'id' => 'numeric',
            'fields' => 'required|array',
            'fields.*' => 'array',
            'fields.*.key'=> 'required|string',
            'fields.*.value'=> 'required|nullable',
        ];
    }
}
