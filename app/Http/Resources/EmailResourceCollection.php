<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class EmailResourceCollection
 * @package App\Http\Resources
 */
class EmailResourceCollection extends ResourceCollection
{
    /**
     * @var string
     */
    public static $wrap='emails';
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
