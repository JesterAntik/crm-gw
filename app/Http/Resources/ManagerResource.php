<?php

namespace App\Http\Resources;

use App\Models\UONManager;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

/**
 * Class ManagerResource
 * @package App\Http\Resources
 * @mixin UONManager
 */
class ManagerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);

        $result['full_name'] = "$this->u_surname ".$this->u_name;

        return $result;
    }
}
