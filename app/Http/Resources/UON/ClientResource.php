<?php

namespace App\Http\Resources\UON;

use App\Http\Resources\AddressResource;
use App\Http\Resources\EmailResourceCollection;
use App\Http\Resources\PhoneResource;
use App\Models\UONClient;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ClientResource
 * @package App\Http\Resources\UON
 * @mixin UONClient
 */
class ClientResource extends JsonResource
{
    /**
     * @var string
     */
    public static $wrap = "client";

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {

        $result = parent::toArray($request);
        $result['birthday'] = $this->when(!empty($this->birthday), function () {
            return $this->birthday->format('d.m.Y');
        });
        $result['zagran_given'] = $this->when(!empty($this->zagran_given), function () {
            return $this->zagran_given->format('d.m.Y');
        });
        $result['zagran_expired'] = $this->when(!empty($this->zagran_expired), function () {
            return $this->zagran_expired->format('d.m.Y');
        });
        $result['emails'] = new EmailResourceCollection($this->emails);
        $result['phone'] = $this->when(!empty($this->phone), function () {
            return new PhoneResource($this->phone);
        });
        $result['mobile_phone'] = $this->when(!empty($this->mobile_phone), function () {
            return new PhoneResource($this->mobile_phone);
        });

        $result['home_phone'] = $this->when(!empty($this->home_phone), function () {
            return new PhoneResource($this->home_phone);
        });

        $result['address'] = $this->when(!empty($this->address), function(){
           return new AddressResource($this->address);
        });

        $result['legal_address'] = $this->when(!empty($this->legal_address), function(){
           return new AddressResource($this->legal_address);
        });

        $result['full_name'] = "$this->u_surname $this->u_name $this->u_sname";
        return $result;
    }
}
