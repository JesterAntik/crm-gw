<?php

namespace App\Http\Resources\UON;

use App\Models\UONDeal;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class DealResourceCollection
 * @package App\Http\Resources\UON
 * @mixin UONDeal
 */
class DealResourceCollection extends ResourceCollection
{
    /**
     * @var string
     */
    public static $wrap = 'deals';
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
