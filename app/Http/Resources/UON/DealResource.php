<?php

namespace App\Http\Resources\UON;

use App\Http\Resources\CountryResource;
use App\Http\Resources\CountryResourceCollection;
use App\Http\Resources\CurrencyResource;
use App\Http\Resources\EmailResourceCollection;
use App\Http\Resources\ManagerResource;
use App\Http\Resources\ServiceResourceCollection;
use App\Http\Resources\SourceResource;
use App\Http\Resources\StatusResource;
use App\Http\Resources\SupplierResource;
use App\Http\Resources\TravelTypeResource;
use App\Models\UONCountry;
use App\Models\UONDeal;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class DealResource
 * @package App\Http\Resources\UON
 * @mixin UONDeal
 */
class DealResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);
       $result['dat'] = $this->when(!empty($this->dat), function () {
            return $this->dat->format('d.m.Y');
        });
        $result['dat_request'] = $this->when(!empty($this->dat_request), function () {
            return $this->dat_request->format('d.m.Y');
        });
        $result['date_begin'] = $this->when(!empty($this->date_begin), function () {
            return $this->date_begin->format('d.m.Y');
        });
        $result['date_end'] = $this->when(!empty($this->date_end), function () {
            return $this->date_end->format('d.m.Y');
        });
         $result['dat_updated'] = $this->when(!empty($this->dat_updated), function () {
            return Carbon::parse($this->dat_updated)->format('d.m.Y H:i');
        });
         $result['created_at'] = $this->when(!empty($this->created_at), function () {
            return Carbon::parse($this->created_at)->format('d.m.Y H:i');
        });

         $result['services'] = new ServiceResourceCollection($this->services);

         $result['supplier'] = new SupplierResource($this->supplier);

         $result['manager'] = new ManagerResource($this->manager);

         $result['client'] = new ClientResource($this->client);

         $result['source'] = new SourceResource($this->source);

         $result['travel_type'] = new TravelTypeResource($this->travelType);
         $result['status'] = new StatusResource($this->status);

         $result['client_currency'] = new CurrencyResource($this->clientCurrency);
         $result['partner_currency'] = new CurrencyResource($this->partnerCurrency);

         $result['client_requirements_country'] =
             $this->when(!empty($this->client_requirements_country_ids),function(){
                 $ids =explode('|',$this->client_requirements_country_ids);
             return new CountryResourceCollection(
                 UONCountry::whereIn('id',$ids)->get());
         });

         $result['client_requirements_date_from'] = $this->when($this->client_requirements_date_from,function(){
             return Carbon::parse($this->client_requirements_date_from)->format('d.m.Y');
         });

        return $result;
    }
}
