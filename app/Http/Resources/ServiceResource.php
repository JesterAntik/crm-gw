<?php

namespace App\Http\Resources;

use App\Models\UONService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ServiceResource
 * @package App\Http\Resources
 * @mixin UONService
 */
class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);

        $result['date_begin'] = $this->when(!empty($this->date_begin), function () {
            return $this->date_begin->format('d.m.Y');
        });
        $result['date_end'] = $this->when(!empty($this->date_end), function () {
            return $this->date_end->format('d.m.Y');
        });

        $result['type'] = new ServiceTypeResource($this->serviceType);
        return $result;
    }
}
