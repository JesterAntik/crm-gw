<?php

namespace App\Models;


use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;


/**
 * App\Models\UONClient
 *
 * @property int $id
 * @property int $global_u_id
 * @property int $u_id
 * @property int|null $u_type
 * @property int|null $tourist_kind
 * @property string|null $u_surname
 * @property string|null $u_name
 * @property string|null $u_sname
 * @property Collection|array|null $clean_result
 * @property string|null $u_surname_en
 * @property string|null $u_name_en
 * @property string|null $u_email
 * @property array|Collection|null $email_validation
 * @property int|null $u_sex
 * @property string|null $u_fax
 * @property string|null $u_phone
 * @property string|null $u_phone_mobile
 * @property string|null $u_phone_home
 * @property string|null $u_passport_number
 * @property string|null $u_passport_taken
 * @property string|null $u_passport_date
 * @property string|null $u_zagran_number
 * @property string|null $u_zagran_given
 * @property string|null $u_zagran_expire
 * @property string|null $u_zagran_organization
 * @property string|null $u_birthday
 * @property string|null $u_birthday_place
 * @property string|null $u_birthday_certificate
 * @property int|null $manager_id
 * @property int|null $u_manager_id
 * @property string|null $u_social_vk
 * @property string|null $u_social_fb
 * @property string|null $u_social_ok
 * @property string|null $u_telegram
 * @property string|null $u_whatsapp
 * @property string|null $u_viber
 * @property string|null $u_instagram
 * @property string|null $u_discount_card_number
 * @property string|null $u_discount_card_bonus
 * @property string|null $u_image
 * @property string|null $u_note
 * @property string|null $timezone
 * @property string|null $u_position
 * @property int|null $u_manager_status
 * @property string|null $u_company
 * @property string|null $u_inn
 * @property string|null $u_kpp
 * @property string|null $u_okved
 * @property string|null $u_ogrn
 * @property string|null $u_finance_bank
 * @property string|null $u_finance_rs
 * @property string|null $u_finance_ks
 * @property string|null $u_finance_bik
 * @property string|null $u_finance_okpo
 * @property string|null $delivery_subscription
 * @property int|null $role_id
 * @property int|null $office_id
 * @property int|null $u_office_id
 * @property int $active
 * @property int $deleted
 * @property string|null $ip_internal_number
 * @property string|null $labels
 * @property string|null $u_labels
 * @property string|null $u_date_update
 * @property int $cabinet_created
 * @property string|null $cabinet_created_date
 * @property int|null $nationality_id
 * @property string|null $nationality
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|UONClient newModelQuery()
 * @method static Builder|UONClient newQuery()
 * @method static Builder|UONClient query()
 * @method static Builder|UONClient whereActive($value)
 * @method static Builder|UONClient whereAddress($value)
 * @method static Builder|UONClient whereAddressJuridical($value)
 * @method static Builder|UONClient whereCabinetCreated($value)
 * @method static Builder|UONClient whereCabinetCreatedDate($value)
 * @method static Builder|UONClient whereCreatedAt($value)
 * @method static Builder|UONClient whereDeleted($value)
 * @method static Builder|UONClient whereDeliverySubscription($value)
 * @method static Builder|UONClient whereGlobalUId($value)
 * @method static Builder|UONClient whereId($value)
 * @method static Builder|UONClient whereIpInternalNumber($value)
 * @method static Builder|UONClient whereIsRealEmail($value)
 * @method static Builder|UONClient whereIsValidEmail($value)
 * @method static Builder|UONClient whereLabels($value)
 * @method static Builder|UONClient whereManagerId($value)
 * @method static Builder|UONClient whereNationality($value)
 * @method static Builder|UONClient whereNationalityId($value)
 * @method static Builder|UONClient whereOfficeId($value)
 * @method static Builder|UONClient whereRoleId($value)
 * @method static Builder|UONClient whereTimezone($value)
 * @method static Builder|UONClient whereTouristKind($value)
 * @method static Builder|UONClient whereUBirthday($value)
 * @method static Builder|UONClient whereUBirthdayCertificate($value)
 * @method static Builder|UONClient whereUBirthdayPlace($value)
 * @method static Builder|UONClient whereUCompany($value)
 * @method static Builder|UONClient whereUDateUpdate($value)
 * @method static Builder|UONClient whereUDiscountCardBonus($value)
 * @method static Builder|UONClient whereUDiscountCardNumber($value)
 * @method static Builder|UONClient whereUEmail($value)
 * @method static Builder|UONClient whereUFax($value)
 * @method static Builder|UONClient whereUFinanceBank($value)
 * @method static Builder|UONClient whereUFinanceBik($value)
 * @method static Builder|UONClient whereUFinanceKs($value)
 * @method static Builder|UONClient whereUFinanceOkpo($value)
 * @method static Builder|UONClient whereUFinanceRs($value)
 * @method static Builder|UONClient whereUId($value)
 * @method static Builder|UONClient whereUImage($value)
 * @method static Builder|UONClient whereUInn($value)
 * @method static Builder|UONClient whereUInstagram($value)
 * @method static Builder|UONClient whereUKpp($value)
 * @method static Builder|UONClient whereULabels($value)
 * @method static Builder|UONClient whereUManagerId($value)
 * @method static Builder|UONClient whereUManagerStatus($value)
 * @method static Builder|UONClient whereUName($value)
 * @method static Builder|UONClient whereUNameEn($value)
 * @method static Builder|UONClient whereUNote($value)
 * @method static Builder|UONClient whereUOfficeId($value)
 * @method static Builder|UONClient whereUOgrn($value)
 * @method static Builder|UONClient whereUOkved($value)
 * @method static Builder|UONClient whereUPassportDate($value)
 * @method static Builder|UONClient whereUPassportNumber($value)
 * @method static Builder|UONClient whereUPassportTaken($value)
 * @method static Builder|UONClient whereUPhone($value)
 * @method static Builder|UONClient whereUPhoneHome($value)
 * @method static Builder|UONClient whereUPhoneMobile($value)
 * @method static Builder|UONClient whereUPosition($value)
 * @method static Builder|UONClient whereUSex($value)
 * @method static Builder|UONClient whereUSname($value)
 * @method static Builder|UONClient whereUSocialFb($value)
 * @method static Builder|UONClient whereUSocialOk($value)
 * @method static Builder|UONClient whereUSocialVk($value)
 * @method static Builder|UONClient whereUSurname($value)
 * @method static Builder|UONClient whereUSurnameEn($value)
 * @method static Builder|UONClient whereUTelegram($value)
 * @method static Builder|UONClient whereUType($value)
 * @method static Builder|UONClient whereUViber($value)
 * @method static Builder|UONClient whereUWhatsapp($value)
 * @method static Builder|UONClient whereUZagranExpire($value)
 * @method static Builder|UONClient whereUZagranGiven($value)
 * @method static Builder|UONClient whereUZagranNumber($value)
 * @method static Builder|UONClient whereUZagranOrganization($value)
 * @method static Builder|UONClient whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string|null $passport_date
 * @property string|null $full_name
 * @property Carbon|null $zagran_given
 * @property Carbon|null $zagran_expired
 * @property Carbon|null $birthday
 * @property-read UONManager|null $manager
 * @property-read \Illuminate\Database\Eloquent\Collection|Email[] $emails
 * @method static Builder|UONClient whereBirthday($value)
 * @method static Builder|UONClient whereCleanResult($value)
 * @method static Builder|UONClient whereEmailValidation($value)
 * @method static Builder|UONClient wherePassportDate($value)
 * @method static Builder|UONClient whereZagranExpired($value)
 * @method static Builder|UONClient whereZagranGiven($value)
 * @property string|null $address_juridical
 * @property int|null $address_id
 * @property int|null $legal_address_id
 * @property int|null $phone_id
 * @property int|null $phone_mobile_id
 * @property int|null $phone_home_id
 * @property int|null $amo_id
 * @property-read Address|null $address
 * @property-read Phone|null $home_phone
 * @property-read Address|null $legal_address
 * @property-read Phone|null $mobile_phone
 * @property-read Phone|null $phone
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UONClient whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UONClient whereAmoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UONClient whereLegalAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UONClient wherePhoneHomeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UONClient wherePhoneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UONClient wherePhoneMobileId($value)
 */
class UONClient extends Client
{
    /**
     * @var string
     */
    protected $table = 'uon_clients';

    /**
     * @var array
     */
    protected $dates = [
        'passportDate',
        'zagran_given',
        'zagran_expired',
        'birthday',
    ];
    /**
     * @var array
     */
    protected $casts = [
        'email_validation' => 'Collection',
        'clean_result' => 'Collection',
    ];

    public function getFullNameAttribute()
    {
        return "$this->u_surname $this->u_name $this->u_sname";
    }
    /**
     * @return BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(UONManager::class, 'manager_id', 'u_id');
    }

    /**
     * @return BelongsToMany
     */
    public function emails()
    {
        return $this->belongsToMany(Email::class,'client_email','client_id','email_id','u_id','id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class,'address_id','id');
    }

    public function legal_address()
    {
        return $this->belongsTo(Address::class,'legal_address_id','id');
    }

    public function phone()
    {
        return $this->belongsTo(Phone::class,'phone_id','id');
    }

    public function mobile_phone()
    {
        return $this->belongsTo(Phone::class,'phone_mobile_id','id');
    }

    public function home_phone()
    {
        return $this->belongsTo(Phone::class,'phone_home_id','id');
    }
}
