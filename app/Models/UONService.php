<?php

namespace App\Models;

use App\Collections\ServiceCollection;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class UONService
 *
 * @package App\Models
 * @property-read UONCurrency $currency
 * @property-read UONDeal $request
 * @property-read UONServiceType $serviceType
 * @method static Builder|UONService newModelQuery()
 * @method static Builder|UONService newQuery()
 * @method static Builder|UONService query()
 * @mixin Eloquent
 * @property int $id
 * @property int|null $request_id
 * @property int|null $lead_id
 * @property Carbon|null $date_begin
 * @property Carbon|null $date_end
 * @property string|null $description
 * @property int $in_package
 * @property int $service_type_id
 * @property string|null $course
 * @property string|null $duration
 * @property float|null $price_netto
 * @property int|null $rate_netto
 * @property float|null $price
 * @property int|null $rate
 * @property int $currency_id
 * @property int|null $currency_id_netto
 * @property float|null $discount
 * @property float $discount_in_percent
 * @property int $tourists_count
 * @property int $tourists_child_count
 * @property int $tourists_baby_count
 * @property int|null $country_id
 * @property int|null $city_id
 * @property int|null $hotel_id
 * @property int|null $nutrition_id
 * @property int|null $partner_id
 * @method static Builder|UONService whereCityId($value)
 * @method static Builder|UONService whereCountryId($value)
 * @method static Builder|UONService whereCourse($value)
 * @method static Builder|UONService whereCurrencyId($value)
 * @method static Builder|UONService whereCurrencyIdNetto($value)
 * @method static Builder|UONService whereDateBegin($value)
 * @method static Builder|UONService whereDateEnd($value)
 * @method static Builder|UONService whereDescription($value)
 * @method static Builder|UONService whereDiscount($value)
 * @method static Builder|UONService whereDiscountInPercent($value)
 * @method static Builder|UONService whereDuration($value)
 * @method static Builder|UONService whereHotelId($value)
 * @method static Builder|UONService whereId($value)
 * @method static Builder|UONService whereInPackage($value)
 * @method static Builder|UONService whereLeadId($value)
 * @method static Builder|UONService whereNutritionId($value)
 * @method static Builder|UONService wherePartnerId($value)
 * @method static Builder|UONService wherePrice($value)
 * @method static Builder|UONService wherePriceNetto($value)
 * @method static Builder|UONService whereRate($value)
 * @method static Builder|UONService whereRateNetto($value)
 * @method static Builder|UONService whereRequestId($value)
 * @method static Builder|UONService whereServiceTypeId($value)
 * @method static Builder|UONService whereTouristsBabyCount($value)
 * @method static Builder|UONService whereTouristsChildCount($value)
 * @method static Builder|UONService whereTouristsCount($value)
 */
class UONService extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_services';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    protected $dates = [
        'date_begin',
        'date_end',
    ];
    /**
     * @return BelongsTo
     */
    public function serviceType()
    {
        return $this->belongsTo(UONServiceType::class, 'service_type_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(UONCurrency::class, 'currency_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function request()
    {
        return $this->belongsTo(UONDeal::class, 'request_id', 'id_system');
    }

    /**
     * @param array $models
     * @return ServiceCollection|Collection
     */
    public function newCollection(array $models = [])
    {
        return ServiceCollection::make($models);
    }
}
