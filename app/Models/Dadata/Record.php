<?php


namespace App\Models\Dadata;


use JsonSerializable;
use Str;

class Record extends AbstractResponse implements JsonSerializable
{
    private $list = [];

    public function attach(AbstractResponse $item)
    {
        $this->list[] = $item;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $list = [];
        foreach ($this->list as $item) {
            $serialized = json_decode(json_encode($item),true);
            $class_parts =explode('\\',get_class($item));
            $serialized['record_type'] = Str::upper( array_pop($class_parts));
            $list[] = $serialized;
        }
        return $list;
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }

}
