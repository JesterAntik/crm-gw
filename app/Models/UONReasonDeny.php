<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONReasonDeny
 *
 * @method static Builder|UONReasonDeny newModelQuery()
 * @method static Builder|UONReasonDeny newQuery()
 * @method static Builder|UONReasonDeny query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $color
 * @property int $ord
 * @property string $note
 * @method static Builder|UONReasonDeny whereColor($value)
 * @method static Builder|UONReasonDeny whereId($value)
 * @method static Builder|UONReasonDeny whereName($value)
 * @method static Builder|UONReasonDeny whereNote($value)
 * @method static Builder|UONReasonDeny whereOrd($value)
 */
class UONReasonDeny extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_reason_denies';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
