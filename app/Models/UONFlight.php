<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONFlight
 *
 * @method static Builder|UONFlight newModelQuery()
 * @method static Builder|UONFlight newQuery()
 * @method static Builder|UONFlight query()
 * @mixin Eloquent
 * @property int $id
 * @property string|null $date_begin
 * @property string|null $date_end
 * @property string|null $time_begin
 * @property string|null $time_end
 * @property string|null $flight_number
 * @property string|null $course_begin
 * @property string|null $course_end
 * @property string|null $terminal_begin
 * @property string|null $terminal_end
 * @property string|null $code_begin
 * @property string|null $code_end
 * @property string|null $seats
 * @property string|null $tickets
 * @property string|null $type
 * @property string|null $class
 * @property string|null $duration
 * @property int|null $supplier_id
 * @method static Builder|UONFlight whereClass($value)
 * @method static Builder|UONFlight whereCodeBegin($value)
 * @method static Builder|UONFlight whereCodeEnd($value)
 * @method static Builder|UONFlight whereCourseBegin($value)
 * @method static Builder|UONFlight whereCourseEnd($value)
 * @method static Builder|UONFlight whereDateBegin($value)
 * @method static Builder|UONFlight whereDateEnd($value)
 * @method static Builder|UONFlight whereDuration($value)
 * @method static Builder|UONFlight whereFlightNumber($value)
 * @method static Builder|UONFlight whereId($value)
 * @method static Builder|UONFlight whereSeats($value)
 * @method static Builder|UONFlight whereSupplierId($value)
 * @method static Builder|UONFlight whereTerminalBegin($value)
 * @method static Builder|UONFlight whereTerminalEnd($value)
 * @method static Builder|UONFlight whereTickets($value)
 * @method static Builder|UONFlight whereTimeBegin($value)
 * @method static Builder|UONFlight whereTimeEnd($value)
 * @method static Builder|UONFlight whereType($value)
 */
class UONFlight extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_flights';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
