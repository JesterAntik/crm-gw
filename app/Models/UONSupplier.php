<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class UONSupplier
 *
 * @package App\Models
 * @property-read UONSupplierType $supplierType
 * @method static Builder|UONSupplier newModelQuery()
 * @method static Builder|UONSupplier newQuery()
 * @method static Builder|UONSupplier query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $name_official
 * @property int $type_id
 * @property string|null $address
 * @property string|null $phones
 * @property string|null $inn
 * @property string|null $email
 * @property string|null $contacts
 * @property string|null $note
 * @property int $is_active
 * @method static Builder|UONSupplier whereAddress($value)
 * @method static Builder|UONSupplier whereContacts($value)
 * @method static Builder|UONSupplier whereEmail($value)
 * @method static Builder|UONSupplier whereId($value)
 * @method static Builder|UONSupplier whereInn($value)
 * @method static Builder|UONSupplier whereIsActive($value)
 * @method static Builder|UONSupplier whereName($value)
 * @method static Builder|UONSupplier whereNameOfficial($value)
 * @method static Builder|UONSupplier whereNote($value)
 * @method static Builder|UONSupplier wherePhones($value)
 * @method static Builder|UONSupplier whereTypeId($value)
 */
class UONSupplier extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_suppliers';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function supplierType()
    {
        return $this->belongsTo(UONSupplierType::class,'supplier_type_id','id');
    }
}
