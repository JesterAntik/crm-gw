<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONCountry
 *
 * @method static Builder|UONCountry newModelQuery()
 * @method static Builder|UONCountry newQuery()
 * @method static Builder|UONCountry query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @method static Builder|UONCountry whereId($value)
 * @method static Builder|UONCountry whereName($value)
 * @method static Builder|UONCountry whereNameEn($value)
 */
class UONCountry extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_countries';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
