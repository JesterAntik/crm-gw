<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONCompany
 *
 * @method static Builder|UONCompany newModelQuery()
 * @method static Builder|UONCompany newQuery()
 * @method static Builder|UONCompany query()
 * @mixin Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $fullname
 * @property string|null $name_rus
 * @property string|null $inn
 * @method static Builder|UONCompany whereFullname($value)
 * @method static Builder|UONCompany whereId($value)
 * @method static Builder|UONCompany whereInn($value)
 * @method static Builder|UONCompany whereName($value)
 * @method static Builder|UONCompany whereNameRus($value)
 */
class UONCompany extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_companies';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
