<?php

namespace App\Models;


use App\Collections\ServiceCollection;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class UONDeal
 *
 * @package App\Models
 * @property-read ServiceCollection|UONService[] $services
 * @method static Builder|UONDeal newModelQuery()
 * @method static Builder|UONDeal newQuery()
 * @method static Builder|UONDeal query()
 * @mixin Eloquent
 * @property int $id
 * @property int $id_system
 * @property string $id_internal
 * @property string|null $reservation_number
 * @property int $supplier_id
 * @property string|null $dat
 * @property string|null $dat_request
 * @property string|null $dat_lead
 * @property int|null $office_id
 * @property int|null $manager_id
 * @property int|null $visa_id
 * @property int|null $insurance_id
 * @property int $client_id
 * @property Carbon|null $date_begin
 * @property Carbon|null $date_end
 * @property int|null $source_id
 * @property int|null $travel_type_id
 * @property int|null $status_id
 * @property float|null $calc_price_netto
 * @property float|null $calc_price
 * @property int|null $r_calc_partner_currency_id
 * @property int|null $r_calc_client_currency_id
 * @property float|null $calc_increase
 * @property float|null $calc_decrease
 * @property float|null $calc_client
 * @property float|null $calc_partner
 * @property string|null $client_requirements_country_ids
 * @property string|null $client_requirements_date_from
 * @property string|null $client_requirements_date_to
 * @property string|null $client_requirements_days_from
 * @property string|null $client_requirements_days_to
 * @property string|null $client_requirements_hotel_stars
 * @property string|null $client_requirements_nutrition_ids
 * @property string|null $client_requirements_tourists_adult_count
 * @property string|null $client_requirements_tourists_child_count
 * @property string|null $client_requirements_tourists_child_age
 * @property string|null $client_requirements_tourists_baby_count
 * @property string|null $client_requirements_budget
 * @property string|null $client_requirements_note
 * @property string $dat_updated
 * @property string $created_at
 * @property int|null $created_by_manager
 * @property string|null $notes
 * @property int|null $bonus_limit
 * @property int|null $company_id
 * @property string|null $payment_deadline_client
 * @property string|null $dat_close
 * @method static Builder|UONDeal whereBonusLimit($value)
 * @method static Builder|UONDeal whereCalcClient($value)
 * @method static Builder|UONDeal whereCalcDecrease($value)
 * @method static Builder|UONDeal whereCalcIncrease($value)
 * @method static Builder|UONDeal whereCalcPartner($value)
 * @method static Builder|UONDeal whereCalcPrice($value)
 * @method static Builder|UONDeal whereCalcPriceNetto($value)
 * @method static Builder|UONDeal whereClientId($value)
 * @method static Builder|UONDeal whereClientRequirementsBudget($value)
 * @method static Builder|UONDeal whereClientRequirementsCountryIds($value)
 * @method static Builder|UONDeal whereClientRequirementsDateFrom($value)
 * @method static Builder|UONDeal whereClientRequirementsDateTo($value)
 * @method static Builder|UONDeal whereClientRequirementsDaysFrom($value)
 * @method static Builder|UONDeal whereClientRequirementsDaysTo($value)
 * @method static Builder|UONDeal whereClientRequirementsHotelStars($value)
 * @method static Builder|UONDeal whereClientRequirementsNote($value)
 * @method static Builder|UONDeal whereClientRequirementsNutritionIds($value)
 * @method static Builder|UONDeal whereClientRequirementsTouristsAdultCount($value)
 * @method static Builder|UONDeal whereClientRequirementsTouristsBabyCount($value)
 * @method static Builder|UONDeal whereClientRequirementsTouristsChildAge($value)
 * @method static Builder|UONDeal whereClientRequirementsTouristsChildCount($value)
 * @method static Builder|UONDeal whereCompanyId($value)
 * @method static Builder|UONDeal whereCreatedAt($value)
 * @method static Builder|UONDeal whereCreatedByManager($value)
 * @method static Builder|UONDeal whereDat($value)
 * @method static Builder|UONDeal whereDatClose($value)
 * @method static Builder|UONDeal whereDatLead($value)
 * @method static Builder|UONDeal whereDatRequest($value)
 * @method static Builder|UONDeal whereDatUpdated($value)
 * @method static Builder|UONDeal whereDateBegin($value)
 * @method static Builder|UONDeal whereDateEnd($value)
 * @method static Builder|UONDeal whereId($value)
 * @method static Builder|UONDeal whereIdInternal($value)
 * @method static Builder|UONDeal whereIdSystem($value)
 * @method static Builder|UONDeal whereInsuranceId($value)
 * @method static Builder|UONDeal whereManagerId($value)
 * @method static Builder|UONDeal whereNotes($value)
 * @method static Builder|UONDeal whereOfficeId($value)
 * @method static Builder|UONDeal wherePaymentDeadlineClient($value)
 * @method static Builder|UONDeal whereRCalcClientCurrencyId($value)
 * @method static Builder|UONDeal whereRCalcPartnerCurrencyId($value)
 * @method static Builder|UONDeal whereReservationNumber($value)
 * @method static Builder|UONDeal whereSourceId($value)
 * @method static Builder|UONDeal whereStatusId($value)
 * @method static Builder|UONDeal whereSupplierId($value)
 * @method static Builder|UONDeal whereTravelTypeId($value)
 * @method static Builder|UONDeal whereVisaId($value)
 * @property-read \App\Models\UONClient $client
 * @property-read \App\Models\UONCurrency|null $clientCurrency
 * @property-read \App\Models\UONManager|null $manager
 * @property-read \App\Models\UONCurrency|null $partnerCurrency
 * @property-read \App\Models\UONSource|null $source
 * @property-read \App\Models\UONStatus|null $status
 * @property-read \App\Models\UONSupplier $supplier
 * @property-read \App\Models\UONTravelType|null $travelType
 */
class UONDeal extends Deal
{
    /**
     * @var string
     */
    protected $table = 'uon_requests';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $dates = [
        'dat',
        'dat_request',
        'date_begin',
        'date_end',
        //  'dat_updated',
        //  'created_at',
    ];

    public function supplier()
    {
        return $this->belongsTo(UONSupplier::class, 'supplier_id', 'id');
    }

    public function manager()
    {
        return $this->belongsTo(UONManager::class, 'manager_id', 'u_id');
    }

    public function client()
    {
        return $this->belongsTo(UONClient::class, 'client_id', 'u_id');
    }

    public function source()
    {
        return $this->belongsTo(UONSource::class, 'source_id', 'rs_id');
    }

    public function travelType()
    {
        return $this->belongsTo(UONTravelType::class, 'travel_type_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(UONStatus::class, 'status_id', 'id');
    }

    public function partnerCurrency()
    {
        return $this->belongsTo(UONCurrency::class, 'r_calc_partner_currency_id', 'id');
    }

    public function clientCurrency()
    {
        return $this->belongsTo(UONCurrency::class, 'r_calc_client_currency_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function services()
    {
        return $this->hasMany(UONService::class, 'request_id', 'id');
    }

}
