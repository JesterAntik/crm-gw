<?php

namespace App\Models;

use App\Collections\DealCollection;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class UONManager
 *
 * @package App\Models
 * @property-read DealCollection|UONDeal[] $requests
 * @method static Builder|UONManager newModelQuery()
 * @method static Builder|UONManager newQuery()
 * @method static Builder|UONManager query()
 * @mixin Eloquent
 * @property int $u_id
 * @property int $global_u_id
 * @property int|null $u_type
 * @property int|null $tourist_kind
 * @property string|null $u_surname
 * @property string|null $u_name
 * @property string|null $u_sname
 * @property string|null $u_surname_en
 * @property string|null $u_name_en
 * @property string|null $u_email
 * @property int|null $u_sex
 * @property string|null $u_fax
 * @property string|null $u_phone
 * @property string|null $u_phone_mobile
 * @property string|null $u_phone_home
 * @property string|null $u_passport_number
 * @property string|null $u_passport_taken
 * @property string|null $u_passport_date
 * @property string|null $u_zagran_number
 * @property string|null $u_zagran_given
 * @property string|null $u_zagran_expire
 * @property string|null $u_zagran_organization
 * @property string|null $u_birthday
 * @property string|null $u_birthday_place
 * @property string|null $u_birthday_certificate
 * @property int|null $manager_id
 * @property int|null $u_manager_id
 * @property string|null $u_social_vk
 * @property string|null $u_social_fb
 * @property string|null $u_social_ok
 * @property string|null $u_telegram
 * @property string|null $u_whatsapp
 * @property string|null $u_viber
 * @property string|null $u_instagram
 * @property string|null $u_discount_card_number
 * @property string|null $u_discount_card_bonus
 * @property string|null $u_image
 * @property string|null $u_note
 * @property string|null $timezone
 * @property string|null $u_position
 * @property int|null $u_manager_status
 * @property string|null $u_company
 * @property string|null $u_inn
 * @property string|null $u_kpp
 * @property string|null $u_okved
 * @property string|null $u_ogrn
 * @property string|null $address
 * @property string|null $address_juridical
 * @property string|null $u_finance_bank
 * @property string|null $u_finance_rs
 * @property string|null $u_finance_ks
 * @property string|null $u_finance_bik
 * @property string|null $u_finance_okpo
 * @property string|null $delivery_subscription
 * @property int|null $role_id
 * @property int|null $office_id
 * @property int|null $u_office_id
 * @property int $active
 * @property int $deleted
 * @property string|null $ip_internal_number
 * @property string|null $labels
 * @property string|null $u_labels
 * @property string|null $u_date_update
 * @property int $cabinet_created
 * @property string|null $cabinet_created_date
 * @property int|null $nationality_id
 * @property string|null $nationality
 * @method static Builder|UONManager whereActive($value)
 * @method static Builder|UONManager whereAddress($value)
 * @method static Builder|UONManager whereAddressJuridical($value)
 * @method static Builder|UONManager whereCabinetCreated($value)
 * @method static Builder|UONManager whereCabinetCreatedDate($value)
 * @method static Builder|UONManager whereDeleted($value)
 * @method static Builder|UONManager whereDeliverySubscription($value)
 * @method static Builder|UONManager whereGlobalUId($value)
 * @method static Builder|UONManager whereIpInternalNumber($value)
 * @method static Builder|UONManager whereLabels($value)
 * @method static Builder|UONManager whereManagerId($value)
 * @method static Builder|UONManager whereNationality($value)
 * @method static Builder|UONManager whereNationalityId($value)
 * @method static Builder|UONManager whereOfficeId($value)
 * @method static Builder|UONManager whereRoleId($value)
 * @method static Builder|UONManager whereTimezone($value)
 * @method static Builder|UONManager whereTouristKind($value)
 * @method static Builder|UONManager whereUBirthday($value)
 * @method static Builder|UONManager whereUBirthdayCertificate($value)
 * @method static Builder|UONManager whereUBirthdayPlace($value)
 * @method static Builder|UONManager whereUCompany($value)
 * @method static Builder|UONManager whereUDateUpdate($value)
 * @method static Builder|UONManager whereUDiscountCardBonus($value)
 * @method static Builder|UONManager whereUDiscountCardNumber($value)
 * @method static Builder|UONManager whereUEmail($value)
 * @method static Builder|UONManager whereUFax($value)
 * @method static Builder|UONManager whereUFinanceBank($value)
 * @method static Builder|UONManager whereUFinanceBik($value)
 * @method static Builder|UONManager whereUFinanceKs($value)
 * @method static Builder|UONManager whereUFinanceOkpo($value)
 * @method static Builder|UONManager whereUFinanceRs($value)
 * @method static Builder|UONManager whereUId($value)
 * @method static Builder|UONManager whereUImage($value)
 * @method static Builder|UONManager whereUInn($value)
 * @method static Builder|UONManager whereUInstagram($value)
 * @method static Builder|UONManager whereUKpp($value)
 * @method static Builder|UONManager whereULabels($value)
 * @method static Builder|UONManager whereUManagerId($value)
 * @method static Builder|UONManager whereUManagerStatus($value)
 * @method static Builder|UONManager whereUName($value)
 * @method static Builder|UONManager whereUNameEn($value)
 * @method static Builder|UONManager whereUNote($value)
 * @method static Builder|UONManager whereUOfficeId($value)
 * @method static Builder|UONManager whereUOgrn($value)
 * @method static Builder|UONManager whereUOkved($value)
 * @method static Builder|UONManager whereUPassportDate($value)
 * @method static Builder|UONManager whereUPassportNumber($value)
 * @method static Builder|UONManager whereUPassportTaken($value)
 * @method static Builder|UONManager whereUPhone($value)
 * @method static Builder|UONManager whereUPhoneHome($value)
 * @method static Builder|UONManager whereUPhoneMobile($value)
 * @method static Builder|UONManager whereUPosition($value)
 * @method static Builder|UONManager whereUSex($value)
 * @method static Builder|UONManager whereUSname($value)
 * @method static Builder|UONManager whereUSocialFb($value)
 * @method static Builder|UONManager whereUSocialOk($value)
 * @method static Builder|UONManager whereUSocialVk($value)
 * @method static Builder|UONManager whereUSurname($value)
 * @method static Builder|UONManager whereUSurnameEn($value)
 * @method static Builder|UONManager whereUTelegram($value)
 * @method static Builder|UONManager whereUType($value)
 * @method static Builder|UONManager whereUViber($value)
 * @method static Builder|UONManager whereUWhatsapp($value)
 * @method static Builder|UONManager whereUZagranExpire($value)
 * @method static Builder|UONManager whereUZagranGiven($value)
 * @method static Builder|UONManager whereUZagranNumber($value)
 * @method static Builder|UONManager whereUZagranOrganization($value)
 */
class UONManager extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_managers';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return HasMany
     */
    public function requests()
    {
        return $this->hasMany(UONDeal::class,'manager_id','u_id');
    }
}
