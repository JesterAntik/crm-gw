<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONOffice
 *
 * @method static Builder|UONOffice newModelQuery()
 * @method static Builder|UONOffice newQuery()
 * @method static Builder|UONOffice query()
 * @mixin Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $city
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $phones
 * @property int|null $company_id
 * @method static Builder|UONOffice whereAddress($value)
 * @method static Builder|UONOffice whereCity($value)
 * @method static Builder|UONOffice whereCompanyId($value)
 * @method static Builder|UONOffice whereId($value)
 * @method static Builder|UONOffice whereName($value)
 * @method static Builder|UONOffice wherePhone($value)
 * @method static Builder|UONOffice wherePhones($value)
 */
class UONOffice extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_offices';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
