<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONSource
 *
 * @method static Builder|UONSource newModelQuery()
 * @method static Builder|UONSource newQuery()
 * @method static Builder|UONSource query()
 * @mixin Eloquent
 * @property int $rs_id
 * @property string $rs_name
 * @property int $is_active
 * @method static Builder|UONSource whereIsActive($value)
 * @method static Builder|UONSource whereRsId($value)
 * @method static Builder|UONSource whereRsName($value)
 */
class UONSource extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_sources';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
