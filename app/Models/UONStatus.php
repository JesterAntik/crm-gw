<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONStatus
 *
 * @method static Builder|UONStatus newModelQuery()
 * @method static Builder|UONStatus newQuery()
 * @method static Builder|UONStatus query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @method static Builder|UONStatus whereId($value)
 * @method static Builder|UONStatus whereName($value)
 */
class UONStatus extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_statuses';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
