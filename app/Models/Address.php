<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Address
 *
 * @property int $id
 * @property string $hash
 * @property string $full_address
 * @property string|null $postal_code
 * @property string|null $country
 * @property string|null $country_iso_code
 * @property string|null $federal_district
 * @property string|null $region_fias_id
 * @property string|null $region_kladr_id
 * @property string|null $region_iso_code
 * @property string|null $region_with_type
 * @property string|null $region_type
 * @property string|null $region_type_full
 * @property string|null $region
 * @property string|null $area_fias_id
 * @property string|null $area_kladr_id
 * @property string|null $area_with_type
 * @property string|null $area_type
 * @property string|null $area_type_full
 * @property string|null $area
 * @property string|null $city_fias_id
 * @property string|null $city_kladr_id
 * @property string|null $city_with_type
 * @property string|null $city_type
 * @property string|null $city_type_full
 * @property string|null $city
 * @property string|null $city_area
 * @property string|null $city_district_fias_id
 * @property string|null $city_district_kladr_id
 * @property string|null $city_district_with_type
 * @property string|null $city_district_type
 * @property string|null $city_district_type_full
 * @property string|null $city_district
 * @property string|null $settlement_fias_id
 * @property string|null $settlement_kladr_id
 * @property string|null $settlement_with_type
 * @property string|null $settlement_type
 * @property string|null $settlement_type_full
 * @property string|null $settlement
 * @property string|null $street_fias_id
 * @property string|null $street_kladr_id
 * @property string|null $street_with_type
 * @property string|null $street_type
 * @property string|null $street_type_full
 * @property string|null $street
 * @property string|null $house_fias_id
 * @property string|null $house_kladr_id
 * @property string|null $house_type
 * @property string|null $house_type_full
 * @property string|null $house
 * @property string|null $block_type
 * @property string|null $block_type_full
 * @property string|null $block
 * @property string|null $flat_type
 * @property string|null $flat_type_full
 * @property string|null $flat
 * @property string|null $flat_area
 * @property string|null $square_meter_price
 * @property string|null $flat_price
 * @property string|null $postal_box
 * @property string|null $fias_id
 * @property string|null $fias_code
 * @property int|null $fias_level
 * @property int|null $fias_actuality_state
 * @property string|null $kladr_id
 * @property int|null $capital_marker
 * @property string|null $okato
 * @property string|null $oktmo
 * @property string|null $tax_office
 * @property string|null $tax_office_legal
 * @property string|null $timezone
 * @property string|null $geo_lat
 * @property string|null $geo_lon
 * @property string|null $beltway_hit
 * @property string|null $beltway_distance
 * @property string|null $qc_geo
 * @property string|null $qc_complete
 * @property string|null $qc_house
 * @property string|null $qc
 * @property string|null $unparsed_parts
 * @property mixed|null $metro
 * @property string|null $result
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaFiasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaKladrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaTypeFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAreaWithType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereBeltwayDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereBeltwayHit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereBlock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereBlockType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereBlockTypeFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCapitalMarker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityDistrictFiasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityDistrictKladrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityDistrictType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityDistrictTypeFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityDistrictWithType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityFiasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityKladrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityTypeFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCityWithType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCountryIsoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFederalDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFiasActualityState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFiasCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFiasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFiasLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFlat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFlatArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFlatPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFlatType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFlatTypeFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFullAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereGeoLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereGeoLon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereHouseFiasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereHouseKladrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereHouseType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereHouseTypeFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereKladrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereMetro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereOkato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereOktmo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address wherePostalBox($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereQc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereQcComplete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereQcGeo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereQcHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereRegionFiasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereRegionIsoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereRegionKladrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereRegionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereRegionTypeFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereRegionWithType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereSettlement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereSettlementFiasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereSettlementKladrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereSettlementType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereSettlementTypeFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereSettlementWithType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereSquareMeterPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereStreetFiasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereStreetKladrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereStreetType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereStreetTypeFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereStreetWithType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereTaxOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereTaxOfficeLegal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereUnparsedParts($value)
 * @mixin \Eloquent
 */
class Address extends Model
{
    /**
     * @var string
     */
    protected $table = 'addresses';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    public static function generateHash($address)
    {
        $address = trim($address);
        $address = preg_replace('/^(\d{6},)/', '', $address);
        $address = preg_replace('/[ \s,.]/', '', $address);
        return sha1($address);
    }

    public function setFullAddressAttribute($value)
    {
        $this->attributes['full_address'] = $value;
        $this->attributes['hash'] = self::generateHash($value);
    }
}
