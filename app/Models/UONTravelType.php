<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONTravelType
 *
 * @method static Builder|UONTravelType newModelQuery()
 * @method static Builder|UONTravelType newQuery()
 * @method static Builder|UONTravelType query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @method static Builder|UONTravelType whereId($value)
 * @method static Builder|UONTravelType whereName($value)
 */
class UONTravelType extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_travel_types';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
