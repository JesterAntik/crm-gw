<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UONHotelType extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_hotel_types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
