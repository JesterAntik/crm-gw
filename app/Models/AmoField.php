<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AmoField
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $type
 * @property int $element_type
 * @property int|null $amo_id
 * @property int|null $is_active
 * @property int|null $is_deleted
 * @property array|null $values
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereAmoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereElementType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AmoField whereValues($value)
 * @mixin \Eloquent
 */
class AmoField extends Model
{
    protected $table='amo_fields';
    protected $casts = [
        'values' => 'array',
    ];
    protected $fillable = [
        'is_deleted',
        'amo_id',
    ];
}
