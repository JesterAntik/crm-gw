<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONSupplierType
 *
 * @method static Builder|UONSupplierType newModelQuery()
 * @method static Builder|UONSupplierType newQuery()
 * @method static Builder|UONSupplierType query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @method static Builder|UONSupplierType whereId($value)
 * @method static Builder|UONSupplierType whereName($value)
 */
class UONSupplierType extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_supplier_types';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
