<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UONCurrency
 *
 * @package App\Models
 * @method static Builder|UONCurrency newModelQuery()
 * @method static Builder|UONCurrency newQuery()
 * @method static Builder|UONCurrency query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @method static Builder|UONCurrency whereId($value)
 * @method static Builder|UONCurrency whereName($value)
 */
class UONCurrency extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_currencies';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
