<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONNutrition
 *
 * @method static Builder|UONNutrition newModelQuery()
 * @method static Builder|UONNutrition newQuery()
 * @method static Builder|UONNutrition query()
 * @mixin Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $name_en
 * @method static Builder|UONNutrition whereId($value)
 * @method static Builder|UONNutrition whereName($value)
 * @method static Builder|UONNutrition whereNameEn($value)
 */
class UONNutrition extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_nutritions';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
