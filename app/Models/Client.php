<?php

namespace App\Models;

use App\Collections\ClientCollection;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Client
 *
 * @method static Builder|Client newModelQuery()
 * @method static Builder|Client newQuery()
 * @method static Builder|Client query()
 * @mixin Eloquent
 */
class Client extends Model
{
    protected $guarded = [];
    public function newCollection(array $models = [])
    {
        return ClientCollection::make($models);
    }
}
