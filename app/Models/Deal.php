<?php

namespace App\Models;

use App\Collections\DealCollection;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Deal
 *
 * @method static Builder|Deal newModelQuery()
 * @method static Builder|Deal newQuery()
 * @method static Builder|Deal query()
 * @mixin Eloquent
 */
class Deal extends Model
{
    public function newCollection(array $models = [])
    {
        return DealCollection::make($models);
    }
}
