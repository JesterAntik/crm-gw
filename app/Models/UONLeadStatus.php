<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONLeadStatus
 *
 * @method static Builder|UONLeadStatus newModelQuery()
 * @method static Builder|UONLeadStatus newQuery()
 * @method static Builder|UONLeadStatus query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @method static Builder|UONLeadStatus whereId($value)
 * @method static Builder|UONLeadStatus whereName($value)
 */
class UONLeadStatus extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_lead_statuses';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
