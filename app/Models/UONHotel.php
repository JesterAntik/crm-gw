<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONHotel
 *
 * @method static Builder|UONHotel newModelQuery()
 * @method static Builder|UONHotel newQuery()
 * @method static Builder|UONHotel query()
 * @mixin Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $name_en
 * @property string|null $stars
 * @property int|null $country_id
 * @property int|null $city_id
 * @property int|null $bedroom_count
 * @property int|null $guests_count
 * @property float|null $price
 * @property string|null $text
 * @property string|null $reserve_rules
 * @property string|null $exit_hour
 * @property string|null $contacts
 * @property string|null $notice
 * @method static Builder|UONHotel whereBedroomCount($value)
 * @method static Builder|UONHotel whereCityId($value)
 * @method static Builder|UONHotel whereContacts($value)
 * @method static Builder|UONHotel whereCountryId($value)
 * @method static Builder|UONHotel whereExitHour($value)
 * @method static Builder|UONHotel whereGuestsCount($value)
 * @method static Builder|UONHotel whereId($value)
 * @method static Builder|UONHotel whereName($value)
 * @method static Builder|UONHotel whereNameEn($value)
 * @method static Builder|UONHotel whereNotice($value)
 * @method static Builder|UONHotel wherePrice($value)
 * @method static Builder|UONHotel whereReserveRules($value)
 * @method static Builder|UONHotel whereStars($value)
 * @method static Builder|UONHotel whereText($value)
 */
class UONHotel extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_hotels';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
