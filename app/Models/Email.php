<?php

namespace App\Models;

use App\Collections\ClientCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Email
 *
 * @property int $id
 * @property string $address
 * @property string|null $alt
 * @property int|null $is_valid
 * @property int $is_real
 * @property-read ClientCollection|UONClient[] $clients
 * @method static Builder|\App\Models\Email newModelQuery()
 * @method static Builder|\App\Models\Email newQuery()
 * @method static Builder|\App\Models\Email query()
 * @method static Builder|\App\Models\Email whereAddress($value)
 * @method static Builder|\App\Models\Email whereAlt($value)
 * @method static Builder|\App\Models\Email whereId($value)
 * @method static Builder|\App\Models\Email whereIsReal($value)
 * @method static Builder|\App\Models\Email whereIsValid($value)
 * @mixin \Eloquent
 */
class Email extends Model
{
    /**
     * @var string
     */
    protected $table = 'emails';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
    protected $casts = [
        'is_valid' => 'boolean',
        'is_real' => 'boolean',
    ];

    public function clients()
    {
        return $this->belongsToMany(UONClient::class, 'client_email', 'client_id', 'email_id', 'id', 'u_id');
    }
}
