<?php

namespace App\Models;


use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

/**
 * App\Models\UONLead
 *
 * @method static Builder|UONLead newModelQuery()
 * @method static Builder|UONLead newQuery()
 * @method static Builder|UONLead query()
 * @mixin Eloquent
 * @property int $id
 * @property int $id_system
 * @property string|null $id_internal
 * @property string|null $reservation_number
 * @property int|null $supplier_id
 * @property string|null $dat
 * @property string|null $dat_lead
 * @property string|null $dat_close
 * @property int|null $office_id
 * @property int|null $manager_id
 * @property int|null $visa_id
 * @property int|null $insurance_id
 * @property int|null $client_id
 * @property Carbon|null $date_begin
 * @property Carbon|null $date_end
 * @property int|null $source_id
 * @property int|null $travel_type_id
 * @property int|null $status_id
 * @property int|null $reason_deny_id
 * @property float|null $calc_price_netto
 * @property float|null $calc_price
 * @property int|null $r_calc_partner_currency_id
 * @property int|null $r_calc_client_currency_id
 * @property float|null $calc_increase
 * @property float|null $calc_decrease
 * @property float|null $calc_client
 * @property float|null $calc_partner
 * @property string|null $client_requirements_country_ids
 * @property string|null $client_requirements_date_from
 * @property string|null $client_requirements_date_to
 * @property string|null $client_requirements_days_from
 * @property string|null $client_requirements_days_to
 * @property string|null $client_requirements_hotel_stars
 * @property string|null $client_requirements_nutrition_ids
 * @property string|null $client_requirements_tourists_adult_count
 * @property string|null $client_requirements_tourists_child_count
 * @property string|null $client_requirements_tourists_child_age
 * @property string|null $client_requirements_tourists_baby_count
 * @property string|null $client_requirements_budget
 * @property string|null $client_requirements_note
 * @property string|null $dat_request
 * @property string|null $dat_updated
 * @property string|null $created_at
 * @property int|null $created_by_manager
 * @property string|null $notes
 * @property int|null $bonus_limit
 * @property int|null $company_id
 * @property string|null $payment_deadline_client
 * @method static Builder|UONLead whereBonusLimit($value)
 * @method static Builder|UONLead whereCalcClient($value)
 * @method static Builder|UONLead whereCalcDecrease($value)
 * @method static Builder|UONLead whereCalcIncrease($value)
 * @method static Builder|UONLead whereCalcPartner($value)
 * @method static Builder|UONLead whereCalcPrice($value)
 * @method static Builder|UONLead whereCalcPriceNetto($value)
 * @method static Builder|UONLead whereClientId($value)
 * @method static Builder|UONLead whereClientRequirementsBudget($value)
 * @method static Builder|UONLead whereClientRequirementsCountryIds($value)
 * @method static Builder|UONLead whereClientRequirementsDateFrom($value)
 * @method static Builder|UONLead whereClientRequirementsDateTo($value)
 * @method static Builder|UONLead whereClientRequirementsDaysFrom($value)
 * @method static Builder|UONLead whereClientRequirementsDaysTo($value)
 * @method static Builder|UONLead whereClientRequirementsHotelStars($value)
 * @method static Builder|UONLead whereClientRequirementsNote($value)
 * @method static Builder|UONLead whereClientRequirementsNutritionIds($value)
 * @method static Builder|UONLead whereClientRequirementsTouristsAdultCount($value)
 * @method static Builder|UONLead whereClientRequirementsTouristsBabyCount($value)
 * @method static Builder|UONLead whereClientRequirementsTouristsChildAge($value)
 * @method static Builder|UONLead whereClientRequirementsTouristsChildCount($value)
 * @method static Builder|UONLead whereCompanyId($value)
 * @method static Builder|UONLead whereCreatedAt($value)
 * @method static Builder|UONLead whereCreatedByManager($value)
 * @method static Builder|UONLead whereDat($value)
 * @method static Builder|UONLead whereDatClose($value)
 * @method static Builder|UONLead whereDatLead($value)
 * @method static Builder|UONLead whereDatRequest($value)
 * @method static Builder|UONLead whereDatUpdated($value)
 * @method static Builder|UONLead whereDateBegin($value)
 * @method static Builder|UONLead whereDateEnd($value)
 * @method static Builder|UONLead whereId($value)
 * @method static Builder|UONLead whereIdInternal($value)
 * @method static Builder|UONLead whereIdSystem($value)
 * @method static Builder|UONLead whereInsuranceId($value)
 * @method static Builder|UONLead whereManagerId($value)
 * @method static Builder|UONLead whereNotes($value)
 * @method static Builder|UONLead whereOfficeId($value)
 * @method static Builder|UONLead wherePaymentDeadlineClient($value)
 * @method static Builder|UONLead whereRCalcClientCurrencyId($value)
 * @method static Builder|UONLead whereRCalcPartnerCurrencyId($value)
 * @method static Builder|UONLead whereReasonDenyId($value)
 * @method static Builder|UONLead whereReservationNumber($value)
 * @method static Builder|UONLead whereSourceId($value)
 * @method static Builder|UONLead whereStatusId($value)
 * @method static Builder|UONLead whereSupplierId($value)
 * @method static Builder|UONLead whereTravelTypeId($value)
 * @method static Builder|UONLead whereVisaId($value)
 */
class UONLead extends Lead
{
    /**
     * @var string
     */
    protected $table = 'uon_leads';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $dates = [
        'date_begin',
        'date_end',
    ];
}
