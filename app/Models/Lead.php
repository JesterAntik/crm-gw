<?php

namespace App\Models;

use App\Collections\LeadCollection;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Lead
 *
 * @method static Builder|Lead newModelQuery()
 * @method static Builder|Lead newQuery()
 * @method static Builder|Lead query()
 * @mixin Eloquent
 */
class Lead extends Model
{
    public function newCollection(array $models = [])
    {
        return LeadCollection::make($models);
    }
}
