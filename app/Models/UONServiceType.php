<?php

namespace App\Models;

use App\Collections\ServiceCollection;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class UONServiceType
 *
 * @package App\Models
 * @property-read ServiceCollection|UONService[] $services
 * @method static Builder|UONServiceType newModelQuery()
 * @method static Builder|UONServiceType newQuery()
 * @method static Builder|UONServiceType query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @method static Builder|UONServiceType whereId($value)
 * @method static Builder|UONServiceType whereName($value)
 */
class UONServiceType extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_service_types';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return HasMany
     */
    public function services()
    {
        return $this->hasMany(UONService::class, 'service_type_id','id');
    }
}
