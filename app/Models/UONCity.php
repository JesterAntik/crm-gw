<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UONCity
 *
 * @method static Builder|UONCity newModelQuery()
 * @method static Builder|UONCity newQuery()
 * @method static Builder|UONCity query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @method static Builder|UONCity whereId($value)
 * @method static Builder|UONCity whereName($value)
 * @method static Builder|UONCity whereNameEn($value)
 */
class UONCity extends Model
{
    /**
     * @var string
     */
    protected $table = 'uon_cities';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];
}
