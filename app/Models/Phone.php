<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Phone\Phone
 *
 * @property int $id
 * @property string $trim_phone
 * @property string|null $full_phone
 * @property string|null $type
 * @property string|null $phone
 * @property string|null $country_code
 * @property string|null $city_code
 * @property string|null $number
 * @property string|null $extension
 * @property string|null $provider
 * @property string|null $country
 * @property string|null $region
 * @property string|null $city
 * @property string|null $timezone
 * @property string|null $qc_conflict
 * @property string|null $qc
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereCityCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereFullPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereQc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereQcConflict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereTrimPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Phone\Phone whereType($value)
 * @mixin \Eloquent
 */
class Phone extends Model
{
    /**
     * @var string
     */
    protected $table = 'phones';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    public static function trim($phone)
    {
        $phone = trim($phone);
        $phone = preg_replace('/[^\d]/', '', $phone);
        return $phone;
    }

    public function setFullPhoneAttribute($value)
    {
        $this->attributes['full_phone'] = $value;
        $this->attributes['trim_phone'] = self::trim($value);
    }
}
