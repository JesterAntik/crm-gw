<?php

namespace App\Console\Commands;

use App\Services\UON\UONVerifier;
use Illuminate\Console\Command;

class UONVerifyClients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uon:clients:verify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверка валидности всех полей';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param UONVerifier $verifier
     * @return mixed
     */
    public function handle(UONVerifier $verifier)
    {
        $verifier->verifyClients();
    }
}
