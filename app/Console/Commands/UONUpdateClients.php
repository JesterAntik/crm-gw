<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Models\Email;
use App\Models\UONClient as UONClientModel;
use App\Phone\Phone;
use App\Services\UON\UONClient;
use App\Services\UON\UONClientService;
use App\Services\UON\UONFormatter;
use Illuminate\Console\Command;

class UONUpdateClients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uon:clients:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновление имеющихся клиентов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param UONClient $crmClient
     * @param UONClientService $clientService
     * @return mixed
     */
    public function handle(UONClient $crmClient, UONClientService $clientService)
    {
        $page = 1;
        while (!empty($clientsData = $crmClient->getClients($page))) {
            foreach ($clientsData as $clientData) {
                $clientService->updateOrCreate($clientData);
            };
            $page++;
        }
    }
}
