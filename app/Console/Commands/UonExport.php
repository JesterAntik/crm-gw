<?php

namespace App\Console\Commands;

use App\Contracts\CRMExport;
use App\Services\UON\UONExportService;
use Illuminate\Console\Command;

class UonExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uon:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export data from U-On system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param UONExportService $CRMExport
     * @return mixed
     */
    public function handle(UONExportService $CRMExport)
    {
    /*    $CRMExport->exportStatuses();
        $CRMExport->exportLeadStatuses();
        $CRMExport->exportOffices();
        $CRMExport->exportReasonDenies();
        $CRMExport->exportSupplierTypes();
        $CRMExport->exportTravelTypes();
        $CRMExport->exportNutrition();
        $CRMExport->exportCountries();
        $CRMExport->exportCities();
        $CRMExport->exportHotels();
        $CRMExport->exportServiceTypes();
        $CRMExport->exportSources();
        $CRMExport->exportOffices();
        $CRMExport->exportReasonDenies();
        $CRMExport->exportCurrency();
        $CRMExport->exportSuppliers();
        $CRMExport->exportManagers();
*/
   //     $CRMExport->exportClients();
        $CRMExport->exportDeals();
        $CRMExport->exportLeads();
        $CRMExport->exportFlights();
    }
}
