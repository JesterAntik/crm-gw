<?php

namespace App\Console\Commands;

use App\Models\UONClient;
use App\Services\AMO\AMOClientService;
use Illuminate\Console\Command;
use linkprofit\AmoCRM\entities\Contact;
use linkprofit\AmoCRM\entities\CustomField;
use linkprofit\AmoCRM\entities\Value;
use linkprofit\AmoCRM\services\AccountService;
use linkprofit\AmoCRM\services\ContactService;

class AMOFillClients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amo:clients:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(AMOClientService $clientService)
    {
       // UONClient::chunkById(100, function ($clients) use ($clientService) {
  $clients = UONClient::limit(20)->get();
            /** @var UONClient $client */
            foreach ($clients as $client) {
                $clientService->fillClient($client);
            }
            $clientService->import();
            foreach ($clientService->lastResults() as $key=>$entity){
                $clients[$key]->update(['amo_id' => $entity->id]);

            }
//        });

    }
}
