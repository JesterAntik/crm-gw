<?php

namespace App\Console\Commands;

use App\Models\AmoField;
use App\Services\AMO\AMOFieldService;
use Illuminate\Console\Command;
use linkprofit\AmoCRM\entities\Field;
use linkprofit\AmoCRM\RequestHandler;
use linkprofit\AmoCRM\services\FieldService;

class AMOCreateFields extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amo:fields:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param RequestHandler $requestHandler
     * @return mixed
     */
    public function handle(AMOFieldService $fieldService)
    {
        //Удаляем старые поля
        $delFields = AmoField::whereNotNull('amo_id')->where('is_active', false)->where('is_deleted', false)->get();
        foreach ($delFields as $field) {
            $fieldService->del($field);
        }

        $amoFields = AmoField::where('is_active', true)->whereNull('amo_id')->orderBy('id')->get();
        foreach ($amoFields as $amoField) {
            $field = new Field();
            $field->origin = "crm_gw_$amoField->code";
            $field->is_editable = true;
            $field->name = $amoField->name;
            $field->element_type = $amoField->element_type;
            $field->field_type = $amoField->type;
            if (!empty($amoField->values)) {
                $field->linkEnumArray($amoField->values);
            }
            $fieldService->add($field);
        }
        $fieldService->save();
        if ($result = $fieldService->parseResponseToEntities())
            foreach ($fieldService->parseResponseToEntities() as $key => $field) {
                $amoFields[$key]->amo_id = $field->id;
                $amoFields[$key]->save();
            }
        $delFields->each(function ($field) {
            $field->update(['is_deleted' => true, 'amo_id'=>null]);
        });

    }
}
