<?php

namespace App\Services\Dadata;

use App\Models\Dadata\Record;
use App\Models\Dadata\AbstractResponse;
use App\Models\Dadata\Address;
use App\Models\Dadata\Date;
use App\Models\Dadata\Email;
use App\Models\Dadata\Name;
use App\Models\Dadata\Passport;
use App\Models\Dadata\Phone;
use App\Models\Dadata\Vehicle;
use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use ReflectionClass;
use ReflectionProperty;
use RuntimeException;
use Str;

/**
 * Class Client
 */
class ClientClean
{
    /**
     * Исходное значение распознано уверенно. Не требуется ручная проверка
     */
    const QC_OK = 0;
    /**
     * Исходное значение распознано с допущениями или не распознано. Требуется ручная проверка
     */
    const QC_UNSURE = 1;
    /**
     * Исходное значение пустое или заведомо "мусорное"
     */
    const QC_INVALID = 2;

    const METHOD_GET = 'GET';

    const METHOD_POST = 'POST';
    /**
     *
     */
    const CLASS_PATH = 'App\Models\Dadata\\';

    /**
     * @var string
     */
    protected $version = 'v2';

    /**
     * @var string
     */
    protected $baseUrl = 'https://dadata.ru/api';

    protected $baseUrlGeolocation = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/detectAddressByIp';

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var ClientInterface
     */
    protected $httpClient;

    /**
     * @var array
     */
    protected $httpOptions = [];

    public function __construct()
    {
        $this->httpClient = new \GuzzleHttp\Client();
        $this->config = config('dadata');
        foreach ($this->config as $name => $value) {
            $this->$name = $value;
        }
    }

    /**
     * Cleans address.
     *
     * @param string $address
     *
     * @return Address
     * @throws GuzzleException
     * @throws \ReflectionException
     */
    public function cleanAddress($address)
    {
        $response = $this->query($this->prepareUri('clean/address'), [$address]);
        $result = $this->populate(new Address, $response);
        if (!$result instanceof Address) {
            throw new RuntimeException('Unexpected populate result: ' . get_class($result) . '. Expected: ' . Address::class);
        }

        return $result;
    }

    /**
     * Cleans phone.
     *
     * @param string $phone
     *
     * @return Phone
     * @throws GuzzleException
     * @throws \ReflectionException
     */
    public function cleanPhone($phone)
    {
        $response = $this->query($this->prepareUri('clean/phone'), [$phone]);
        $result = $this->populate(new Phone, $response);
        if (!$result instanceof Phone) {
            throw new RuntimeException('Unexpected populate result: ' . get_class($result) . '. Expected: ' . Phone::class);
        }
        return $result;
    }

    /**
     * Cleans passport.
     *
     * @param string $passport
     *
     * @return Passport
     * @throws GuzzleException
     * @throws \ReflectionException
     */
    public function cleanPassport($passport)
    {
        $response = $this->query($this->prepareUri('clean/passport'), [$passport]);
        $result = $this->populate(new Passport(), $response);
        if (!$result instanceof Passport) {
            throw new RuntimeException('Unexpected populate result: ' . get_class($result) . '. Expected: ' . Passport::class);
        }

        return $result;
    }

    /**
     * Cleans name.
     *
     * @param string $name
     *
     * @return Name
     * @throws GuzzleException
     * @throws \ReflectionException
     */
    public function cleanName($name)
    {
        $response = $this->query($this->prepareUri('clean/name'), [$name]);
        $result = $this->populate(new Name(), $response);
        if (!$result instanceof Name) {
            throw new RuntimeException('Unexpected populate result: ' . get_class($result) . '. Expected: ' . Name::class);
        }

        return $result;
    }

    /**
     * Cleans email.
     *
     * @param string $email
     *
     * @return Email
     * @throws GuzzleException
     * @throws \ReflectionException
     */
    public function cleanEmail($email)
    {
        $response = $this->query($this->prepareUri('clean/email'), [$email]);
        $result = $this->populate(new Email, $response);
        if (!$result instanceof Email) {
            throw new RuntimeException('Unexpected populate result: ' . get_class($result) . '. Expected: ' . Email::class);
        }

        return $result;
    }

    /**
     * Cleans date.
     *
     * @param string $date
     *
     * @return Date
     * @throws GuzzleException
     * @throws \ReflectionException
     */
    public function cleanDate($date)
    {
        $response = $this->query($this->prepareUri('clean/birthdate'), [$date]);
        $result = $this->populate(new Date, $response);
        if (!$result instanceof Date) {
            throw new RuntimeException('Unexpected populate result: ' . get_class($result) . '. Expected: ' . Date::class);
        }

        return $result;
    }

    /**
     * Cleans vehicle.
     *
     * @param string $vehicle
     *
     * @return Vehicle
     * @throws GuzzleException
     * @throws \ReflectionException
     */
    public function cleanVehicle($vehicle)
    {
        $response = $this->query($this->prepareUri('clean/vehicle'), [$vehicle]);
        $result = $this->populate(new Vehicle, $response);
        if (!$result instanceof Vehicle) {
            throw new RuntimeException('Unexpected populate result: ' . get_class($result) . '. Expected: ' . Vehicle::class);
        }

        return $result;
    }

    /**
     * Gets balance.
     *
     * @return float
     * @throws GuzzleException
     */
    public function getBalance()
    {
        $response = $this->query($this->prepareUri('profile/balance'), [], self::METHOD_GET);
        return (double)$response;
    }

    /**
     * @param $uri
     * @param array $params
     * @param string $method
     * @return mixed
     * @throws GuzzleException
     */
    private function sendRequest($uri, array $params = [], $method = self::METHOD_POST)
    {
        $request = new Request($method, $uri, [
            'Content-Type' => 'application/json',
            'Authorization' => 'Token ' . $this->token,
            'X-Secret' => $this->secret,
        ], 0 < count($params) ? json_encode($params) : null);
        $response = $this->httpClient->send($request, $this->httpOptions);
        $result = json_decode($response->getBody(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new RuntimeException('Error parsing response: ' . json_last_error_msg());
        }
        return $result;
    }

    /**
     * Requests API.
     *
     * @param string $uri
     * @param array $params
     *
     * @param string $method
     *
     * @return array
     * @throws GuzzleException
     */
    protected function query($uri, array $params = [], $method = self::METHOD_POST)
    {
        $result = $this->sendRequest($uri, $params, $method);

        return array_shift($result);
    }

    /**
     * @param $uri
     * @param array $params
     * @param string $method
     * @return mixed
     * @throws GuzzleException
     */
    protected function recordQuery($uri, array $params = [], $method = self::METHOD_POST)
    {
        $result = $this->sendRequest($uri, $params, $method);

        if (empty($result) || !isset($result["data"])) {
            throw new RuntimeException('Empty result');
        }
        return $result;
    }

    /**
     * Prepares URI for the request.
     *
     * @param string $endpoint
     * @return string
     */
    protected function prepareUri($endpoint)
    {
        return $this->baseUrl . '/' . $this->version . '/' . $endpoint;
    }

    /**
     * Populates object with data.
     *
     * @param AbstractResponse $object
     * @param array $data
     * @return AbstractResponse
     * @throws \ReflectionException
     */
    protected function populate(AbstractResponse $object, array $data)
    {
        $reflect = new ReflectionClass($object);

        $properties = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);

        foreach ($properties as $property) {
            if (array_key_exists($property->name, $data)) {
                $object->{$property->name} = $this->getValueByAnnotatedType($property, $data[$property->name]);
            }
        }

        return $object;
    }

    /**
     * Guesses and converts property type by phpdoc comment.
     *
     * @param ReflectionProperty $property
     * @param mixed $value
     * @return mixed
     */
    protected function getValueByAnnotatedType(ReflectionProperty $property, $value)
    {
        $comment = $property->getDocComment();
        if (preg_match('/@var (.+?)(\|null)? /', $comment, $matches)) {
            switch ($matches[1]) {
                case 'integer':
                case 'int':
                    $value = (int)$value;
                    break;
                case 'float':
                    $value = (float)$value;
                    break;
            }
        }

        return $value;
    }

    /**
     * @param string $ip
     * @return null|Address
     * @throws Exception
     * @throws GuzzleException
     */
    public function detectAddressByIp($ip)
    {
        $request = new Request('get', $this->baseUrlGeolocation . '?ip=' . $ip, [
            'Accept' => 'application/json',
            'Authorization' => 'Token ' . $this->token,
        ]);

        $response = $this->httpClient->send($request);

        $result = json_decode($response->getBody(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new RuntimeException('Error parsing response: ' . json_last_error_msg());
        }

        if (!array_key_exists('location', $result)) {
            throw new Exception('Required key "location" is missing');
        }

        if (null === $result['location']) {
            return null;
        }

        if (!array_key_exists('data', $result['location'])) {
            throw new Exception('Required key "data" is missing');
        }

        if (null === $result['location']['data']) {
            return null;
        }

        $address = $this->populate(new Address, $result['location']['data']);
        if (!$address instanceof Address) {
            throw new RuntimeException('Unexpected populate result: ' . get_class($result) . '. Expected: ' . Address::class);
        }

        return $address;
    }

    /**
     * @param $structure
     * @param $record
     * @return Record
     * @throws GuzzleException
     * @throws \ReflectionException
     */
    public function cleanRecord($structure, $record)
    {
        $requestData = array(
            "structure" => $structure,
            "data" => array($record)
        );
        $response = $this->recordQuery($this->prepareUri('clean'), $requestData);
        $result = $this->populateRecord($response);
        if (!$result instanceof Record) {
            throw new RuntimeException('Unexpected populate result: ' . get_class($result) . '. Expected: ' . Record::class);
        }

        return $result;
    }

    /**
     * @param $type
     * @return mixed
     */
    private function getFieldObject($type)
    {
        $class_name = self::CLASS_PATH . Str::ucfirst(Str::lower($type));
        return new $class_name();
    }

    /**
     * @param $source
     * @return Record
     * @throws \ReflectionException
     */
    public function populateRecord($source)
    {
        $record = new Record();
        if (!isset($source['structure']) || !is_array($source['structure']))
            throw new RuntimeException('Incorrect structure format');

        $structure = $source['structure'];
        if (!isset($source['data']) || !is_array($source['data']))
            throw new RuntimeException('Incorrect data format');

        $data = array_shift($source['data']);
        foreach ($structure as $key => $type) {
            $record->attach($this->populate($this->getFieldObject($type), $data[$key]));
        }

        return $record;
    }
}
