<?php


namespace App\Services\UON;


use App\Models\Address;
use App\Models\Email;
use App\Models\UONClient as UONClientModel;
use App\Models\Phone;

class UONClientService
{
    /**
     * @var UONFormatter
     */
    private $formatter;

    public function __construct(UONFormatter $formatter)
    {
        $this->formatter = $formatter;
    }

    public function updateOrCreate($clientData)
    {
        $emails = collect();
        if (!empty($clientData->u_email)) {
            $emailsData = $this->formatter->prepareEmails($clientData->u_email);
            foreach ($emailsData as $emailData) {
                $emails->push(Email::firstOrCreate(['address' => $emailData['address']], $emailData));

            }
        }
        $address = null;
        if (isset($clientData->address)) {
            $addressData = $this->formatter->prepareAddress($clientData->address);
            if (!empty($addressData))
                $address = Address::firstOrCreate(['hash' => $addressData['hash']], $addressData);
        }


        $legal_address = null;
        if (!empty($clientData->address_juridical)) {
            $ur_addressData = $this->formatter->prepareAddress($clientData->address_juridical);
            if (!empty($ur_addressData))
                $legal_address = Address::firstOrCreate(['hash' => $ur_addressData['hash']], $ur_addressData);
        }
        $phone = null;
        if (!empty($clientData->u_phone)) {
            $phone_data = $this->formatter->preparePhone($clientData->u_phone);
            if (!empty($phone_data))
                $phone = Phone::firstOrCreate(['trim_phone' => $phone_data['trim_phone']], $phone_data);
        }

        $mobile_phone = null;
        if (!empty($clientData->u_phone_mobile)) {
            $mobile_phone_data = $this->formatter->preparePhone($clientData->u_phone_mobile);
            if (!empty($mobile_phone_data))
                $mobile_phone = Phone::firstOrCreate(['trim_phone' => $mobile_phone_data['trim_phone']], $mobile_phone_data);
        }

        $home_phone = null;
        if (!empty($clientData->u_phone_home)) {
            $home_phone_data = $this->formatter->preparePhone($clientData->u_phone_home);
            if (!empty($home_phone_data))
                $home_phone = Phone::firstOrCreate(['trim_phone' => $home_phone_data['trim_phone']], $home_phone_data);
        }

        $clientData = $this->formatter->prepareClient($clientData);
        $client = UONClientModel::updateOrCreate(['global_u_id' => $clientData['global_u_id']], $clientData);

        $client->emails()->sync($emails->pluck('id')->values());
        $client->address()->associate($address);
        $client->legal_address()->associate($legal_address);
        $client->phone()->associate($phone);
        $client->mobile_phone()->associate($mobile_phone);
        $client->home_phone()->associate($home_phone);
        $client->save();
    }
}
