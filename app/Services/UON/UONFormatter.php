<?php


namespace App\Services\UON;


use App\Models\Address;
use App\Models\Phone;
use Illuminate\Support\Carbon;
use stdClass as stdClassAlias;

/**
 * Class UONFormatter
 * @package App\Services\UON
 */
class UONFormatter
{
    /**
     * @param stdClassAlias $srcObject
     * @return array
     */
    public function prepareClient($srcObject)
    {
        $clientData = get_object_vars($srcObject);
        //     dump( $srcObject->u_passport_date);
        if (isset($srcObject->u_passport_date)) {
            $clientData['passport_date'] = Carbon::parse($srcObject->u_passport_date);
            unset($clientData['u_passport_date']);
        }

        if (isset($srcObject->u_zagran_given)) {
            $clientData['zagran_given'] = Carbon::parse($srcObject->u_zagran_given);
            unset($clientData['u_zagran_given']);
        }
        if (isset($srcObject->u_zagran_expired)) {
            $clientData['zagran_expired'] = Carbon::parse($srcObject->u_zagran_expired);
            unset($clientData['u_zagran_expired']);
        }

        if (isset($srcObject->u_birthday)) {
            $clientData['birthday'] = Carbon::parse($srcObject->u_birthday);
            unset($clientData['u_birthday']);
        }
        if (isset($srcObject->u_email)) {
            unset($clientData['u_email']);
        }
        if (isset($srcObject->address)) {
            unset($clientData['address']);
        }

        if (isset($srcObject->address_juridical)) {
            unset($clientData['address_juridical']);
        }

        if (isset($srcObject->u_phone)) {
            unset($clientData['u_phone']);
        }

        if (isset($srcObject->u_phone_mobile)) {
            unset($clientData['u_phone_mobile']);
        }

        if (isset($srcObject->u_phone_home)) {
            unset($clientData['u_phone_home']);
        }
        return $clientData;
    }

    public function prepareEmails($u_email)
    {
        if (empty($u_email)) return [];
        $emails = preg_split("/[ ,;\/]/", $u_email);
        $emails = array_filter($emails);
        return array_map(function ($email) {
            return [
                'address' => trim($email),
            ];
        }, $emails);
    }

    public function prepareAddress($address)
    {
        if (empty($address))  return null;
        return [
            'hash' => Address::generateHash($address),
            'full_address' => $address,
        ];
    }

    public function preparePhone($phone)
    {
        if (empty($phone)) return null;
        return [
            'trim_phone' => Phone::trim($phone),
            'full_phone' => $phone,
        ];
    }
}
