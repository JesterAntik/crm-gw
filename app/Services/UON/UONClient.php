<?php


namespace App\Services\UON;


use App\Contracts\CRMClient;
use App\Models\Client;
use UON\API;
use UON\Exceptions\ConfigException;

class UONClient implements CRMClient
{

    private $crmClient = null;

    /**
     * UONClient constructor.
     * @throws ConfigException
     */
    public function __construct()
    {
        $this->crmClient = new API(config('uon.token'));
    }

    public function getClients($page = null): array
    {
        return $this->crmClient->users->all($page)['message']->users;
    }

    public function getDeal($deal_id)
    {
        return $this->crmClient->requests->get($deal_id);
    }

    public function getDeals($client_id = null): array
    {
        return $this->crmClient->requests->getByClient($client_id)['message']->requests;
    }

    public function getLeads($client_id = null, $page = null):array
    {
        $message =$this->crmClient->leads->getByClient($client_id, $page)['message'];
        return empty($message) ? null : $message->leads;
    }

    public function setClients(array $clients)
    {
        // TODO: Implement setClients() method.
    }

    public function setDeals(array $deals)
    {
        // TODO: Implement setDeals() method.
    }

    public function setLeads(array $leads)
    {
        // TODO: Implement setLeads() method.
    }

    public function getCountries()
    {
        return $this->crmClient->countries->all()['message']->records;
    }

    public function getCities($country_id = null, $page = 1)
    {
        if (!empty($country_id)) {
            $message = $this->crmClient->cities->all($country_id, $page)['message'];
            return empty($message) ? null : $message->records;
        }
        return null;
    }

    public function getHotels($page=null)
    {
        $message =$this->crmClient->hotels->all($page)['message'];
        return empty($message) ? null : $message->records;
    }

    public function getServiceStatuses()
    {
        return $this->crmClient->services->getTypes()['message']->items;
    }

    public function getCurrency()
    {
        return $this->crmClient->misc->getCurrency()['message']->records;
    }

    public function getSuppliers($params, $page = null)
    {
        $message = $this->crmClient->suppliers->all($params, $page)['message'];
        return empty($message) ? null : $message->records;
    }

    public function getSupplierTypes()
    {
        return $this->crmClient->suppliers->getTypes()['message']->records;
    }

    public function getTravelTypes()
    {
        return $this->crmClient->requests->getTravelType()['message']->records;
    }

    public function getSources()
    {
        return $this->crmClient->sources->all()['message']->items;
    }

    public function getManagers()
    {
        return $this->crmClient->misc->getManagers()['message']->users;
    }

    public function getRequestStatuses()
    {
        return $this->crmClient->statuses->get()['message']->records;
    }

    public function getLeadStatuses()
    {
        return $this->crmClient->statuses->getLead()['message']->records;
    }

    public function getNutrition()
    {
        return $this->crmClient->nutrition->all()['message']->records;
    }

    public function getOffices()
    {
        return $this->crmClient->misc->getOffices()['message']->records;
    }

    public function getReasonDeny()
    {
        return $this->crmClient->misc->getReasonDeny()['message']->items;
    }

}
