<?php


namespace App\Services\UON;


use App\Contracts\CRMVerifier;
use App\Exceptions\verifyEmailException;
use App\Models\Dadata\Address;
use App\Models\Dadata\Phone;
use App\Models\UONClient;
use App\Services\Dadata\ClientClean;
use App\Services\VerifyEmail;
use GuzzleHttp\Exception\GuzzleException;
use ReflectionException;

/**
 * Class UONVerifier
 * @package App\Services\UON
 */
class UONVerifier implements CRMVerifier
{
    /**
     * @var VerifyEmail
     */
    private $verifyEmailService;
    /**
     * @var ClientClean
     */
    private $dataClean;

    /**
     * UONVerifier constructor.
     * @param VerifyEmail $verifyEmail
     * @param ClientClean $dadataClean
     */
    public function __construct(VerifyEmail $verifyEmail, ClientClean $dadataClean)
    {
        $this->verifyEmailService = $verifyEmail;
        $this->dataClean = $dadataClean;
    }

    /**
     *
     */
    public function verifyClients()
    {
        // UONClient::chunkById(500, function ($clients) {
        $clients = UONClient::whereNull('clean_result')->limit(3)->get();
        foreach ($clients as $client) {
            $this->verifyClient($client);
        }
        //});
    }

    /**
     * @param UONClient $client
     * @throws GuzzleException
     * @throws ReflectionException
     * @throws verifyEmailException
     */
    public function verifyClient($client)
    {
        $this->validateEmail($client);
        $this->verifyComplexData($client);
        $client->save();
    }

    /**
     * @param UONClient $client
     * @throws GuzzleException
     * @throws ReflectionException
     */
    public function verifyComplexData($client)
    {
        $structure = [
            'NAME',
            'PHONE',
            'PHONE',
            'PHONE',
            'ADDRESS',
            'PASSPORT',
            'ADDRESS',
        ];
        $data = [
            "$client->u_surname $client->u_name $client->u_sname",
            !empty($client->phone) ? $client->phone->full_phone : "",
            !empty($client->mobile_phone) ? $client->mobile_phone->full_phone : "",
            !empty($client->home_phone) ? $client->home_phone->full_phone : "",
            !empty($client->address) ? $client->address->full_address : "",
            $client->u_passport_number,
            !empty($client->legal_address) ? $client->legal_address->full_address : "",
        ];
        $client_result = $this->dataClean->cleanRecord($structure, $data)->getList();
        $client->clean_result = [
            "fullname" => $client_result[0],
            //  "phone" => $client_result[1],
            //  "phone_mobile" => $client_result[2],
            //  "phone_home" => $client_result[3],
            //   "address" => $client_result[4],
            "passport" => $client_result[5],
        ];
        if (!empty($client->phone)) $this->populatePhoneResult($client->phone, $client_result[1]);
        if (!empty($client->mobile_phone)) $this->populatePhoneResult($client->mobile_phone, $client_result[2]);
        if (!empty($client->home_phone)) $this->populatePhoneResult($client->home_phone, $client_result[3]);
        if (!empty($client->address)) $this->populateAddressResult($client->address, $client_result[4]);
    }

    public function populatePhoneResult(\App\Models\Phone $phone, Phone $result)
    {
        $phone->fill([
            'type' => $result->type,
            'phone' => $result->phone,
            'country_code' => $result->country_code,
            'city_code' => $result->city_code,
            'number' => $result->number,
            'extension' => $result->extension,
            'provider' => $result->provider,
            'country' => $result->country,
            'region' => $result->region,
            'city' => $result->city,
            'timezone' => $result->timezone,
            'qc_conflict' => $result->qc_conflict,
            'qc' => $result->qc,
        ]);
        $phone->save();
    }

    public function populateAddressResult(\App\Models\Address $address, Address $result)
    {
        $address->fill([
            'result' => $result->result,
            'postal_code' => $result->postal_code,
            'country' => $result->country,
            'country_iso_code' => $result->country_iso_code,
            'federal_district' => $result->federal_district,
            'region_fias_id' => $result->region_fias_id,
            'region_kladr_id' => $result->region_kladr_id,
            'region_iso_code' => $result->region_iso_code,
            'region_with_type' => $result->region_with_type,
            'region_type' => $result->region_type,
            'region_type_full' => $result->region_type_full,
            'region' => $result->region,
            'area_fias_id' => $result->area_fias_id,
            'area_kladr_id' => $result->area_kladr_id,
            'area_with_type' => $result->area_with_type,
            'area_type' => $result->area_type,
            'area_type_full' => $result->area_type_full,
            'area' => $result->area,
            'city_fias_id' => $result->city_fias_id,
            'city_kladr_id' => $result->city_kladr_id,
            'city_with_type' => $result->city_with_type,
            'city_type' => $result->city_type,
            'city_type_full' => $result->city_type_full,
            'city' => $result->city,
            'city_area' => $result->city_area,
            'city_district_fias_id' => $result->city_district_fias_id,
            'city_district_kladr_id' => $result->city_district_kladr_id,
            'city_district_with_type' => $result->city_district_with_type,
            'city_district_type' => $result->city_district_type,
            'city_district_type_full' => $result->city_district_type_full,
            'city_district' => $result->city_district,
            'settlement_fias_id' => $result->settlement_fias_id,
            'settlement_kladr_id' => $result->settlement_kladr_id,
            'settlement_with_type' => $result->settlement_with_type,
            'settlement_type' => $result->settlement_type,
            'settlement_type_full' => $result->settlement_type_full,
            'settlement' => $result->settlement,
            'street_fias_id' => $result->street_fias_id,
            'street_kladr_id' => $result->street_kladr_id,
            'street_with_type' => $result->street_with_type,
            'street_type' => $result->street_type,
            'street_type_full' => $result->street_type_full,
            'street' => $result->street,
            'house_fias_id' => $result->house_fias_id,
            'house_kladr_id' => $result->house_kladr_id,
            'house_type' => $result->house_type,
            'house_type_full' => $result->house_type_full,
            'house' => $result->house,
            'block_type' => $result->block_type,
            'block_type_full' => $result->block_type_full,
            'block' => $result->block,
            'flat_type' => $result->flat_type,
            'flat_type_full' => $result->flat_type_full,
            'flat' => $result->flat,
            'flat_area' => $result->flat_area,
            'square_meter_price' => $result->square_meter_price,
            'flat_price' => $result->flat_price,
            'postal_box' => $result->postal_box,
            'fias_id' => $result->fias_id,
            'fias_code' => $result->fias_code,
            'fias_level' => $result->fias_level,
            'fias_actuality_state' => $result->fias_actuality_state,
            'kladr_id' => $result->kladr_id,
            'capital_marker' => $result->capital_marker,
            'okato' => $result->okato,
            'oktmo' => $result->oktmo,
            'tax_office' => $result->tax_office,
            'tax_office_legal' => $result->tax_office_legal,
            'timezone' => $result->timezone,
            'geo_lat' => $result->geo_lat,
            'geo_lon' => $result->geo_lon,
            'beltway_hit' => $result->beltway_hit,
            'beltway_distance' => $result->beltway_distance,
            'qc_geo' => $result->qc_geo,
            'qc_complete' => $result->qc_complete,
            'qc_house' => $result->qc_house,
            'qc' => $result->qc,
            'unparsed_parts' => $result->unparsed_parts,
            'metro' => $result->metro,
        ]);
        $address->save();

    }


    /**
     * @param UONClient $client
     * @throws verifyEmailException
     */
    public function validateEmail($client)
    {
        if (!empty($client->u_email) && $client->email_validation === null) {
            $emails = preg_split("/[ ,;\/]/", $client->u_email);
            $emails = array_filter($emails, function ($email) {
                return (strlen(trim($email)) > 1 && preg_match('/^.*?@.*$/', $email));
            });
            if (count($emails)) {
                $client->email_validation = collect();
                foreach ($emails as $email) {
                    $result = [];
                    if ($result['is_valid'] = $this->verifyEmailService::validate($email)) {
                        if (!$this->verifyEmailService->check($client->u_email))
                            $result['is_real'] = false;
                    }
                    $client->email_validation->put($email, $result);
                }
            }

            $client->save();
        }

    }
}
