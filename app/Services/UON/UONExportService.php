<?php


namespace App\Services\UON;


use App\Contracts\CRMExport;
use App\Models\UONCity;
use App\Models\UONClient as UONClientModel;
use App\Models\UONCompany;
use App\Models\UONCountry;
use App\Models\UONCurrency;
use App\Models\UONDeal;
use App\Models\UONFlight;
use App\Models\UONHotel;
use App\Models\UONHotelType;
use App\Models\UONLead;
use App\Models\UONLeadStatus;
use App\Models\UONManager;
use App\Models\UONNutrition;
use App\Models\UONOffice;
use App\Models\UONReasonDeny;
use App\Models\UONService;
use App\Models\UONServiceType;
use App\Models\UONSource;
use App\Models\UONStatus;
use App\Models\UONSupplier;
use App\Models\UONSupplierType;
use App\Models\UONTravelType;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Str;

/**
 * Class UONExportService
 * @package App\Services\UON
 */
class UONExportService implements CRMExport
{
    /**
     * @var UONClient
     */
    private $client;
    /**
     * @var UONFormatter
     */
    private $formatter;

    /**
     * UONExportService constructor.
     * @param UONClient $client
     * @param UONFormatter $formatter
     */
    public function __construct(UONClient $client, UONFormatter $formatter)
    {
        $this->client = $client;
        $this->formatter = $formatter;
    }

    /**
     * @return void
     */
    public function exportClients()
    {
        $page = 1;
        while (!empty($clientsData = $this->client->getClients($page))) {
            foreach ($clientsData as $clientData) {
                $clientData = $this->formatter->prepareClient($clientData);
                UONClientModel::updateOrCreate(['global_u_id' => $clientData['global_u_id']], $clientData);
            };
            $page++;
        }
    }

    /**
     * @return void
     */
    public function exportLeads()
    {
        echo "Экспорт лидов \n";
        UONClientModel::chunkById(500, function ($clients) {
            foreach ($clients as $client) {
                $page = 1;
                do {
                    echo "Leads for $client->u_id page $page\n";
                    $leads = $this->client->getLeads($client->u_id, $page);
                    foreach ($leads as $srcData) {
                        $leadData = $this->normalizeRequest($srcData);
                        UONLead::updateOrCreate(['id' => $leadData->id], get_object_vars($leadData));
                        $this->saveServices($srcData, 'lead_id');
                    }
                    $page++;
                } while (count($leads) >= 100);
            }
        });
    }

    /**
     * @return void
     */
    public function exportDeals()
    {
        echo "Экспорт заявок \n";
        UONClientModel::chunkById(500, function ($clients) {
            foreach ($clients as $client) {
                echo "Заявки клиент $client->u_id\n";
                $requests = $this->client->getDeals($client->u_id);
                foreach ($requests as $srcData) {
                    $this->saveRequest($srcData);
                }
            }
        });
    }

    private function normalizeDate($date)
    {
        if (empty($date) || mb_stripos($date, '-') == 0 || $date === '1970-01-01 00:00') {
            $date = null;
        } else
            $date = Carbon::make($date);
        return $date;
    }

    public function normalizeRequest($requestData)
    {
        $requestData = clone $requestData;

        if (!empty($requestData->date_begin))
            $requestData->date_begin = $this->normalizeDate($requestData->date_begin);
        if (!empty($requestData->date_end))
            $requestData->date_end = $this->normalizeDate($requestData->date_end);

        if (!empty($requestData->dat))
            $requestData->dat = $this->normalizeDate($requestData->dat);
        if (!empty($requestData->dat_request))
            $requestData->dat_request = $this->normalizeDate($requestData->dat_request);


        unset($requestData->supplier_name);
        unset($requestData->supplier_inn);

        unset($requestData->manager_surname);
        unset($requestData->manager_sname);
        unset($requestData->manager_name);

        unset($requestData->client_surname);
        unset($requestData->client_name);
        unset($requestData->client_sname);
        unset($requestData->client_phone);
        unset($requestData->client_phone_mobile);
        unset($requestData->client_email);
        unset($requestData->client_company);
        unset($requestData->client_kpp);
        unset($requestData->client_inn);

        unset($requestData->social_vk);
        unset($requestData->social_fb);
        unset($requestData->social_ok);
        unset($requestData->telegram);
        unset($requestData->whatsapp);
        unset($requestData->instagram);
        unset($requestData->viber);

        unset($requestData->supplier_kpp);
        unset($requestData->supplier_inn);


        unset($requestData->travel_type);

        unset($requestData->status);
        unset($requestData->source);

        unset($requestData->company_name);
        unset($requestData->company_fullname);
        unset($requestData->company_name_rus);
        unset($requestData->company_inn);

        unset($requestData->payment_deadline_partner);

        unset($requestData->services);

        return $requestData;
    }

    public function saveRequest($srcData)
    {
        $requestData = $this->normalizeRequest($srcData);
        $request = UONDeal::updateOrCreate(['id' => $requestData->id], get_object_vars($requestData));
        $request->fresh();

        UONCompany::updateOrCreate(['id' => $srcData->company_id], [
            'name' => $srcData->company_name,
            'fullname' => $srcData->company_fullname,
            'name_rus' => $srcData->company_name_rus,
            'inn' => $srcData->company_inn,
        ]);
        $this->saveServices($srcData);
    }

    public function saveServices($srcData, $foreign = "request_id")
    {
        foreach ($srcData->services as $serviceData) {
            $serviceData->$foreign = $srcData->id;
            try {
                if (isset($serviceData->hotel_type_id)) {
                    dump($serviceData->hotel_type_id);
                    UONHotelType::updateOrCreate(['id' => $serviceData->hotel_type_id],
                        [
                            'name' => $serviceData->hotel_type,
                            'name_en' => $serviceData->hotel_type_en,
                        ]
                    );
                }
                $this->saveService($serviceData);
            } catch (Exception $e) {
                dump($serviceData);
                throw $e;
            }
        }

    }

    public function saveService($data)
    {
        unset($data->service_type);
        unset($data->currency);
        unset($data->currency_code);
        unset($data->currency_netto);
        unset($data->currency_code_netto);
        unset($data->country);
        unset($data->country_en);
        unset($data->city);
        unset($data->city_en);
        unset($data->hotel);
        unset($data->hotel_type);
        unset($data->hotel_type_en);
        unset($data->hotel_en);
        unset($data->nutrition);
        unset($data->nutrition_en);
        unset($data->partner_name);
        unset($data->partner_name_en);
        unset($data->partner_kpp);
        unset($data->partner_inn);

        $data->date_begin = $this->normalizeDate($data->date_begin);
        $data->date_end = $this->normalizeDate($data->date_end);
        UONService::updateOrCreate(['id' => $data->id], get_object_vars($data));

    }

    /**
     * @param string $listName
     * @return void
     */
    public function exportList($listName)
    {
        $arguments = func_get_args();
        $listName = array_shift($arguments);
        $methodName = "export" . Str::ucfirst($listName);
        if (method_exists($this, $methodName)) {
            call_user_func_array(array($this, $methodName), $arguments);
        };
    }

    public function exportSuppliers()
    {
        $page = 1;
        while (!empty($suppliersData = $this->client->getSuppliers([], $page))) {
            foreach ($suppliersData as $supplierData) {
                unset($supplierData->type);
                UONSupplier::updateOrCreate(['id' => $supplierData->id], get_object_vars($supplierData));
            };
            $page++;
        }
    }

    public function exportSupplierTypes()
    {
        $supplierTypesData = $this->client->getSupplierTypes();
        foreach ($supplierTypesData as $supplierTypeData) {
            UONSupplierType::updateOrCreate(['id' => $supplierTypeData->id], get_object_vars($supplierTypeData));
        }
    }

    public function exportTravelTypes()
    {
        $travelTypesData = $this->client->getTravelTypes();
        foreach ($travelTypesData as $travelTypeData) {
            UONTravelType::updateOrCreate(['id' => $travelTypeData->id], get_object_vars($travelTypeData));
        }
    }

    public function exportCurrency()
    {
        $currencyData = $this->client->getCurrency();
        foreach ($currencyData as $itemData) {
            UONCurrency::updateOrCreate(['id' => $itemData->id], get_object_vars($itemData));
        }
    }

    public function exportSources()
    {
        $sourcesData = $this->client->getSources();
        foreach ($sourcesData as $itemData) {
            UONSource::updateOrCreate(['rs_id' => $itemData->rs_id], get_object_vars($itemData));
        }
    }

    public function exportOffices()
    {
        $sourcesData = $this->client->getOffices();
        foreach ($sourcesData as $itemData) {
            UONOffice::updateOrCreate(['id' => $itemData->id], get_object_vars($itemData));
        }
    }

    public function exportReasonDenies()
    {
        $sourcesData = $this->client->getReasonDeny();
        foreach ($sourcesData as $itemData) {
            UONReasonDeny::updateOrCreate(['id' => $itemData->id], get_object_vars($itemData));
        }
    }

    public function exportManagers()
    {
        $managersData = $this->client->getManagers();
        foreach ($managersData as $itemData) {
            UONManager::updateOrCreate(['u_id' => $itemData->u_id], get_object_vars($itemData));
        }
    }


    /**
     * @return void
     */
    public function exportServiceTypes()
    {
        $serviceTypesData = $this->client->getServiceStatuses();
        foreach ($serviceTypesData as $serviceTypeData) {
            UONServiceType::updateOrCreate(['id' => $serviceTypeData->id], get_object_vars($serviceTypeData));
        }
    }

    public function exportStatuses()
    {
        $statusesData = $this->client->getRequestStatuses();
        foreach ($statusesData as $statusData) {
            UONStatus::updateOrCreate(['id' => $statusData->id], get_object_vars($statusData));
        }
    }

    public function exportLeadStatuses()
    {
        $statusesData = $this->client->getLeadStatuses();
        foreach ($statusesData as $statusData) {
            UONLeadStatus::updateOrCreate(['id' => $statusData->id], get_object_vars($statusData));
        }
    }

    public function exportCountries()
    {
        $countriesData = $this->client->getCountries();
        foreach ($countriesData as $countryData) {
            UONCountry::updateOrCreate(['id' => $countryData->id], get_object_vars($countryData));
        }
    }

    public function exportCities()
    {
        echo "Экспорт городов \n";
        foreach (UONCountry::all() as $country) {
            $page = 1;
            echo "Получение городов для $country->name \n";
            while ($citiesData = $this->client->getCities($country->id, $page)) {
                foreach ($citiesData as $cityData) {
                    echo "$cityData->name \n";
                    UONCity::updateOrCreate(['id' => $cityData->id], get_object_vars($cityData));
                }
                $page++;
            }

        }
    }

    public function exportHotels()
    {
        echo "Экспорт отелей \n";
        $page = 1;
        while ($hotelsData = $this->client->getHotels($page)) {
            foreach ($hotelsData as $hotelData) {
                echo "$hotelData->name \n";
                unset($hotelData->country);
                unset($hotelData->city);
                UONHotel::updateOrCreate(['id' => $hotelData->id], get_object_vars($hotelData));
            }
            $page++;
        }
    }

    public function exportNutrition()
    {
        $nutritionsData = $this->client->getNutrition();
        foreach ($nutritionsData as $nutritionData) {
            UONNutrition::updateOrCreate(['id' => $nutritionData->id], get_object_vars($nutritionData));
        }
    }

    public function exportFlights()
    {
        echo "Экспорт перелетов \n";
        UONDeal::chunkById(200, function ($requests) {
            foreach ($requests as $request) {
                echo "Получение перелетов для $request->id \n";
                $requestData = $this->client->getDeal($request->id);
                if (!empty($requestData->flights)) {
                    foreach ($requestData->flights as $flight) {
                        UONFlight::updateOrCreate(['id' => $flight->id], get_object_vars($flight));
                    }
                }
            }
        });
    }
}
