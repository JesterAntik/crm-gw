<?php


namespace App\Services\AMO;


use Illuminate\Database\Eloquent\Model;
use linkprofit\AmoCRM\entities\EntityInterface;
use linkprofit\AmoCRM\services\BaseService;

/**
 * Class AMOBaseService
 * @package App\Services\AMO
 */
abstract class AMOBaseService extends BaseService
{
    /**
     * @var array
     */
    private $deleteFields=[];

    /**
     * Fill fields for save request
     */
    protected function composeFields()
    {
        parent::composeFields();
        if (count($this->deleteFields)) {
            $this->fields['delete'] = $this->deleteFields;
        }
    }

    /**
     * @param $field
     */
    public function del($field) {
        if ($field instanceof EntityInterface) {
            $this->deleteFields[] = [
                'id' => $field->id,
                'origin' => $field->origin,
            ];
        }
        if ($field instanceof Model)
            $this->deleteFields[] = [
                'id' => $field->amo_id,
                'origin' =>  "crm_gw_$field->code"
            ];
    }
}
