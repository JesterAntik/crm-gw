<?php


namespace App\Services\AMO;


use linkprofit\AmoCRM\entities\EntityInterface;
use linkprofit\AmoCRM\entities\Field;

class AMOFieldService extends AMOBaseService
{

    /**
     * @var Field[]
     */
    protected $entities = [];

    /**
     * @param EntityInterface $field
     */
    public function add(EntityInterface $field)
    {

        if ($field instanceof Field) {
            $this->entities[] = $field;
        }
    }

    /**
     * @param $array
     * @return Field
     */
    public function parseArrayToEntity($array)
    {
        $field = new Field();
        $field->set($array);

        return $field;
    }

    /**
     * @return string
     */
    protected function getLink()
    {
        return 'https://' . $this->request->getSubdomain() . '.amocrm.ru/api/v2/fields';
    }
}
