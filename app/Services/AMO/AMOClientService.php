<?php


namespace App\Services\AMO;


use AmoFields;
use App\Models\AmoField;
use App\Models\Email;
use App\Models\UONClient;
use App\Phone\Phone;
use linkprofit\AmoCRM\entities\Contact;
use linkprofit\AmoCRM\entities\CustomField;
use linkprofit\AmoCRM\entities\Field;
use linkprofit\AmoCRM\entities\Value;
use linkprofit\AmoCRM\services\AccountService;
use linkprofit\AmoCRM\services\ContactService;

/**
 * Class AMOClientService
 * @package App\Services\AMO
 */
class AMOClientService
{
    /**
     * @var Field[]
     */
    protected $customFields;

    /**
     * @var
     */
    protected $phoneField;

    /**
     * @var
     */
    protected $emailField;
    /**
     * @var ContactService
     */
    private $contactService;

    /**
     * @var
     */
    private $amoFields;
    /**
     * @var Field
     */
    private $genderField;

    /**
     * AMOClientService constructor.
     * @param AccountService $accountService
     * @param ContactService $contactService
     */
    public function __construct(AccountService $accountService, ContactService $contactService)
    {
        $this->customFields = $accountService->getCustomFields();
        $this->contactService = $contactService;
        $this->parseCustom();

        $this->amoFields = AmoField::whereNotNull('amo_id')->where('is_active', true)->get()->pluck('amo_id', 'code');

    }

    private function parseCustom()
    {
        foreach ($this->customFields as $field) {
            if ($field->name == "Телефон") {
                $this->phoneField = $field;
            }
            if ($field->name == "Email") {
                $this->emailField = $field;
            }

            if ($field->name == "Пол") {
                $this->genderField = $field;
            }
        }
    }

    private function getGender($uonValue)
    {
        $literal = $uonValue == 1 ? "м" : "ж";
        foreach ($this->genderField->enums as $id => $value) {
            if ($value == $literal) return $id;
        }
    }

    /**
     * @param Contact $contact
     * @param Phone|null $phone
     * @param string $type
     */
    public function fillPhone(Contact $contact, $phone, string $type = 'OTHER')
    {
        if (!empty($phone)) {
            $newPhoneField = new CustomField($this->phoneField->id);
            $newPhoneField->addValue(new Value($phone->full_phone, $type));
            $contact->addCustomField($newPhoneField);
        }
    }

    public function fillEmail(Contact $contact, Email $email, string $type = 'OTHER')
    {
        if (!empty($email)) {
            $newEmailField = new CustomField($this->emailField->id);
            $newEmailField->addValue(new Value($email->address, $type));
            $contact->addCustomField($newEmailField);
        }
    }

    public function addCustom($contact, $id, $value)
    {
        $field = new CustomField($id);
        $field->addValue(new Value($value));
        $contact->addCustomField($field);
    }

    public function fillClient(UONClient $client)
    {
        $contact = new Contact();
        if (!empty($client->amo_id)) $contact->id = $client->amo_id;
        $contact->name = $client->full_name;
        $this->fillPhone($contact, $client->phone, 'OTHER');
        $this->fillPhone($contact, $client->mobile_phone, 'MOB');
        $this->fillPhone($contact, $client->home_phone, 'HOME');

        foreach ($client->emails as $email) {
            $this->fillEmail($contact, $email, 'OTHER');
        }

        if ($id = $this->amoFields->get('gender', false)) {
            $this->addCustom($contact, $id, $this->getGender($client->u_sex));
        }

        if ($id = $this->amoFields->get('passport_number', false)) {
            $this->addCustom($contact, $id, $client->u_passport_number);
        }

        if ($id = $this->amoFields->get('passport_taken', false)) {
            $this->addCustom($contact, $id, $client->u_passport_taken);
        }

        if ($id = $this->amoFields->get('passport_date', false)) {
            $this->addCustom($contact, $id, $client->passport_date);
        }

        if ($id = $this->amoFields->get('zagran_number', false)) {
            $this->addCustom($contact, $id, $client->u_zagran_number);
        }

        if ($id = $this->amoFields->get('zagran_given', false)) {
            $this->addCustom($contact, $id, $client->zagran_given);
        }

        if ($id = $this->amoFields->get('zagran_expire', false)) {
            $this->addCustom($contact, $id, $client->zagran_expired);
        }

        if ($id = $this->amoFields->get('zagran_organization', false)) {
            $this->addCustom($contact, $id, $client->u_zagran_organization);
        }

        if ($id = $this->amoFields->get('birthday', false)) {
            $this->addCustom($contact, $id, $client->birthday);
        }

        if ($id = $this->amoFields->get('birthday_place', false)) {
            $this->addCustom($contact, $id, $client->u_birthday_place);
        }

        if ($id = $this->amoFields->get('birthday_certificate', false)) {
            $this->addCustom($contact, $id, $client->u_birthday_certificate);
        }

        if ($id = $this->amoFields->get('vk', false)) {
            $this->addCustom($contact, $id, $client->u_social_vk);
        }

        if ($id = $this->amoFields->get('fb', false)) {
            $this->addCustom($contact, $id, $client->u_social_fb);
        }

        if ($id = $this->amoFields->get('ok', false)) {
            $this->addCustom($contact, $id, $client->u_social_ok);
        }

        if ($id = $this->amoFields->get('telegram', false)) {
            $this->addCustom($contact, $id, $client->u_telegram);
        }

        if ($id = $this->amoFields->get('whatsapp', false)) {
            $this->addCustom($contact, $id, $client->u_whatsapp);
        }

        if ($id = $this->amoFields->get('viber', false)) {
            $this->addCustom($contact, $id, $client->u_viber);
        }

        if ($id = $this->amoFields->get('instagram', false)) {
            $this->addCustom($contact, $id, $client->u_instagram);
        }

        if (($id = $this->amoFields->get('address', false)) && !empty($client->address->full_address)) {
            $field = new CustomField($id);

            if (empty($client->address->postal_code)) {
                $address = new Value($client->address->full_address);
                $address->subtype = 'address_line_1';
                $field->addValue($address);
            } else {
                $address = new Value($client->address->full_address);
                $address->subtype = 'address_line_1';
                $field->addValue($address);
                $zip = new Value($client->address->postal_code);
                $zip->subtype = 'zip';
                $field->addValue($zip);
            }

            $contact->addCustomField($field);
        }

        $this->contactService->add($contact);
    }

    public function import()
    {
        $this->contactService->save();
    }

    public function lastResults()
    {
        return $this->contactService->parseResponseToEntities();
    }
}
