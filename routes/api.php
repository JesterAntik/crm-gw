<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->group(function () {
    Route::prefix('uon')->name('uon')->namespace('UON')->group(function () {
        Route::get('/clients', 'ClientController@index')->name('.clients');
        Route::put('/clients/{client}', 'ClientController@update')->name('.clients.update');
        Route::get('/deals', 'DealController@index')->name('.deals');
        Route::get('/deals/failed', 'DealController@failed')->name('.deals.failed');
        Route::put('/deals/{deal}', 'DealController@update')->name('.deals.update');

    });
    Route::prefix('email')->name('email')->group(function(){
        Route::put('{email}','EmailController@update')->name('.update');
    });
    Route::prefix('phone')->name('phone')->group(function(){
        Route::put('{phone}','PhoneController@update')->name('.update');
    });
    Route::prefix('address')->name('address')->group(function(){
        Route::put('{address}','AddressController@update')->name('.update');
    });
});

