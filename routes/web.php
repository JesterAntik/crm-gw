<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/uon/clients',function(){
//   return view('uon.clients');
//});

Auth::routes(['register' => false]);

Route::get('/', 'WebController@index')->name('dashboard');

Route::middleware('auth')->prefix('uon')->name('uon.')->group(function () {
    Route::get('/clients', 'UONController@clients')->name('clients');
    Route::get('/deals', 'UONController@deals')->name('deals');
    Route::get('/deals/failed', 'UONController@incorrectDeals')->name('deals.failed');
});
