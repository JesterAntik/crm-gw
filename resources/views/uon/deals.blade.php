@extends('layouts.page')
@section('js')
    <script src="{{mix('js/deals.js')}}" defer></script>
@endsection
@section('app')
    <deal-list ></deal-list>
@endsection
