@extends('layouts.page')
@section('js')
    <script src="{{mix('js/clients.js')}}" defer></script>
@endsection
@section('app')
    <client-list ></client-list>
@endsection
