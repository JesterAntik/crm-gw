// initial state
const state = {
    user: null,
};

// getters
const getters = {};

// mutations
const mutations = {
    SET_USER: (state, payload) => {
        state.user = payload;
    }
};

// actions
const actions = {
    setUser({dispatch, commit}, payload) {
        if (payload) {
            commit('SET_USER', payload);
            dispatch('setAuthToken', payload.api_token, {root:true});
        } else {
            commit('SET_AUTH_TOKEN', null);
            dispatch('setAuthToken', null, {root:true});
        }
    }
};


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
