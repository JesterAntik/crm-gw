// initial state
import axios from 'axios';
import Routes from '../Routes.json';

const state = {
    Routes,
    http: axios.create({
        baseURL: Routes.baseUrl,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
        }
    }),
};
// getters
const getters = {
    getHttp: (state) => {
        return state.http
    },
    getRoute: (state) => (route_name) => {
        if (!route_name) return state.Routes.default;
        let route = state.Routes;
        let full_path = '';
        route_name.split('.').forEach((route_part) => {
            if (route.hasOwnProperty(route_part)) {
                route = route[route_part];
                if (route.prefix) full_path += '/' + route.prefix;
            }
        });
        route.api = state.Routes.baseApi + full_path + route.uri;
        route.path = full_path;
        return route;
    }

};

// mutations
const mutations = {
    SET_AUTH_TOKEN: (state, token) => {
        state.http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    }
};

// actions
const actions = {
    setAuthToken: {
        root: true,
        handler({commit}, token) {
            commit('SET_AUTH_TOKEN', token);
        }
    }
};


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
