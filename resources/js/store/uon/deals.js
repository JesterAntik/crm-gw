// initial state
const state = {
    failed:false,
};

// getters
const getters = {
    getIsFailed:(state) => state.failed,
};

// actions
const actions = {
    setDealsInfo:({dispatch, commit, getters, rootGetters}, payload) => {
        if (payload.failed) {
            commit('SET_FAILED',true);
        }
    },
};


// mutations
const mutations = {
    SET_FAILED(state,flag) {
      state.failed = flag;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
