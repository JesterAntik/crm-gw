
// initial state
const state = {
};

// getters
const getters = {
};

// actions
const actions = {
};

// mutations
const mutations = {
};
import deals from "./deals";
import clients from "./clients";
export default {
    namespaced: true,
    modules:{
        deals,
        clients,
    },
    state,
    getters,
    actions,
    mutations
}
