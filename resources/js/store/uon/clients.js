// initial state
const state = {
};

// getters
const getters = {
};

// actions
const actions = {
    updateEmail:({dispatch, commit, getters, rootGetters}, payload) => {
        commit('Repository/UPDATE_BY_KEY', {
            index: payload.client.u_id,
            index_name: "u_id",
            fields: [
                {key:"emails", value:payload.emails}
            ],
        }, {root:true});
    },

};


// mutations
const mutations = {
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
