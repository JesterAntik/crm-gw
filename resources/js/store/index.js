import Vue from 'vue'
import Vuex from 'vuex'
import core from './core'
import auth from './auth'
import uon from './uon'
import Repository from './Repository'


Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        core,
        auth,
        Repository,
        uon
    },
    strict: debug,
})
