const state = {
    list: [],
    total: null,
    entity_type: null,
    orderParams: {
        perPage: 15,
        sortBy: 'id',
        sortDesc: false,
    }
};

function checkContext(state_context, current_context) {
    return (state_context.perPage === current_context.perPage
        && state_context.sortBy === current_context.sortBy
        && state_context.sortDesc === current_context.sortDesc)
}

function getByKey(list, value, key) {
    let result = null;
    if (!key) key = 'id';
    list.some(page => {
        return page.some(client => {
            if (client[key] === value) {
                return result = client;
            }
        });
    });
    return result;
}

// getters
const getters = {
    list: state => state.list,
    getContext: (state) => state.orderParams,
    getTotal: (state) => state.total,
    getPage: (state) => (page) => {
        return state.list[page];
    },
    getByKey: (state) => (value, key) => {
        getByKey(state.list, value, key)
    },
    getEntityType: (state) => state.entity_type,
};

const mutations = {
    CLEAR_LIST(state) {
        state.list = [];
    },

    SET_TOTAL: (state, total) => {
        state.total = total
    },

    SET_PARAMS(state, context) {
        state.orderParams.perPage = context.perPage;
        state.orderParams.sortBy = context.sortBy;
        state.orderParams.sortDesc = context.sortDesc;
    },

    SET_ENTITY_TYPE(state, type) {
        state.entity_type = type;
    },

    ADD_TO_LIST(state, payload) {
        state.list[payload.page] = payload.entities;
    },

    UPDATE_BY_KEY(state, payload) {
        let index_key = payload.index_name || 'id';
        state.list.some((page) => {
            return page.some((entity) => {
                if (entity[index_key] === payload.index) {
                    payload.fields.forEach((field) => {
                        entity[field.key] = field.value;
                    });
                }
            });
        });
    }
};

const actions = {
    setEntityType({dispatch, commit}, payload) {
        commit('SET_ENTITY_TYPE', payload);
    },
    loadByPage({dispatch, commit, getters, rootGetters}, payload) {
        let http = rootGetters['core/getHttp'];
        let entity_type = getters.getEntityType;
        let currentPage = payload.context.currentPage;
        let path = payload.context.apiUrl + '?page=' + currentPage
            + '&perPage=' + payload.context.perPage
            + '&sortBy=' + payload.context.sortBy
            + '&sortDesc=' + payload.context.sortDesc;
        http.get(path)
            .then(data => {
                commit('SET_PARAMS', payload.context);
                commit('SET_TOTAL', data.data.meta.total);
                commit('ADD_TO_LIST', {
                    page: currentPage,
                    entities: data.data[entity_type + 's']
                });
                payload.callback(getters.getPage(currentPage));
            })
            .catch(() => {
                //TODO show error
                payload.callback([])
            });
    },
    getByPage: ({dispatch, commit, getters, rootGetters}, payload) => {
        let context = getters.getContext;
        if (checkContext(context, payload.context)) {
            let page_content = getters.getPage(payload.context.currentPage);
            if (!page_content) {
                dispatch('loadByPage', payload);
            } else {
                console.log(page_content);
                payload.callback(page_content);
            }
        } else {
            dispatch('loadByPage', payload);
        }
    },
    updateLocal:({dispatch, commit, getters, rootGetters}, payload) =>{
            commit('UPDATE_BY_KEY', payload);
    },
    updateProperty({dispatch, commit, getters, rootGetters}, payload) {

        let fields = [];
        if (!payload.fields) fields.push(payload);
        else fields = payload.fields;

        if (!payload.local) {
            let http = rootGetters['core/getHttp'];
            let entity_name = getters.getEntityType;
            let route = rootGetters['core/getRoute'](payload["route_name"] || 'uon.' + entity_name + 's.update');

            http({
                method: route.method || 'post',
                url: route.api.replace(':id', payload.id),
                data: {
                    'entity': entity_name,
                    'id': payload.id,
                    fields
                },
            }).then(data => {
                commit('UPDATE_BY_KEY', {
                    index: payload.id,
                    index_name: payload.index_name,
                    fields,
                });
            })
                .catch((error) => {
                    console.error(error);
                    //TODO show error
                });
        } else {
            commit('UPDATE_BY_KEY', {
                index: payload.id,
                index_name: payload.index_name,
                fields,
            });
        }
    },
    updateEntity: ({dispatch, commit, getters, rootGetters}, payload) => {
        let entity_name = getters.getEntityType;
        if (!payload.local) {
            let http = rootGetters['core/getHttp'];
            let route_name = payload.route_name || 'uon.' + entity_name + 's.store';
            let route = rootGetters['core/getRoute'](route_name);
            http({
                method: route.method || 'post',
                url: route.api.replace(':id', payload.id),
                data: payload.entity
            }).then(data => {
                commit('SET_BY_KEY', {
                    index: payload.id,
                    value: data.data[entity_name],
                });
            })
                .catch(() => {
                    //TODO show error
                });
        } else {
            commit('SET_BY_KEY', {
                index: payload.id,
                value: payload.entity,
            });
        }
    }
};


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
