window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
   /* window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');*/

    /*require('bootstrap');*/
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */



window.Vue = require('vue');

import Vuex from 'vuex'
Vue.use(Vuex);
import store from './store'
window.store = store;


import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

Vue.directive("draggable", {
    //adapted from https://codepen.io/kminek/pen/pEdmoo
    inserted: function(el, binding, a) {
        Sortable.create(el, {
            draggable: ".draggable",
            onEnd: function(e) {
                /* vnode.context is the context vue instance: "This is not documented as it's not encouraged to manipulate the vm from directives in Vue 2.0 - instead, directives should be used for low-level DOM manipulation, and higher-level stuff should be solved with components instead. But you can do this if some usecase needs this. */
                // fixme: can this be reworked to use a component?
                // https://github.com/vuejs/vue/issues/4065
                // https://forum.vuejs.org/t/how-can-i-access-the-vm-from-a-custom-directive-in-2-0/2548/3
                // https://github.com/vuejs/vue/issues/2873 "directive interface change"
                // `binding.expression` should be the name of your array from vm.data
                // set the expression like v-draggable="items"
                var clonedItems = a.context[binding.expression].filter(function(item) {
                    return item;
                });
                clonedItems.splice(e.newIndex, 0, clonedItems.splice(e.oldIndex, 1)[0]);
                a.context[binding.expression] = [];
                Vue.nextTick(function() {
                    a.context[binding.expression] = clonedItems;
                });

            }
        });
    }
});

import InitData from './components/InitData';
Vue.component('init-data',InitData);
